![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

[TOC]

![DIY](../../x_gitressourcen/Train_D1.png)![Proof](../../x_gitressourcen/Proof.png)

# Kompetenznachweis I1


# Auftrag: (Gruppenarbeit)

Aufbau und Syntax eines Programms sind bekannt. Selektion und Iterationen (fuss- und kopfgesteuert) können programmiert werden.
Mathematische Operationen (+ - x / % ++ -- += -= *= /= %=) können programmiert werden.

1. Holen Sie bei der Lehrperson den Auftrag (Serie - Task). 
2. Platzieren Sie die entsprechende Planer-Karte vom KN-Feld I1 in ihren Planer "**In progress**".
3. Setzen Sie als Team den Auftrag in *einem* Java-File um.
4. Platzieren Sie die entsprechende Planer-Karte im Planer auf "**To examine**".
5. Informieren Sie die LP und demonstrieren Sie ihr Lösungs-Programm.
6. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" &#8594; 3.

 

## Anforderungen

* **Einfaches Programm** ...
* mit **Eingabemöglichkeit** der nötigen, variablen Daten ("Bitte Seite a eingeben:" x.y), ...
* einem **Berechnungsteil** mit abgespeicherten Resultaten (float resultat := ...) ) und ...
* **Ausgaben** der Resultate, wobei deren Bedeutung angegeben ist. ("Die Fläche ist: 1.21 m^2")


# Proof of competence I1

# Assignment: (group work)

The structure and syntax of a program are known. Selection and iterations (foot and head controlled) can be programmed. Mathematical operations (+ - x / % ++ -- += -= *= /= %=) can be programmed.

* Get the assignment (series - task) from the teacher.
* Place the corresponding planner card from KN field I1 in your " In progress " planner.
* As a team, implement the order in a Java file.
* Place the appropriate planner card on “ To examine ” in the planner.
* Inform the LP and demonstrate your solution program.
* The teacher places the corresponding planner card in the planner on “ Passed ” or “ Redo ” → 3.


## Requirements


* **Simple program** ...
* with the possibility of entering the necessary, variable data (e.g. "Please enter side a:" xy), ...
* a calculation part with saved results (float resultat := ...) ) and ...
* Outputs the results, with their meaning stated. (e.g. "The area is: 1.21 m^2")
