![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

[TOC]

![DIY](../../x_gitressourcen/Train_D1.png)![Proof](../../x_gitressourcen/Proof.png)

# Kompetenznachweis E4


# Code-Review: (Einzelarbeit)

Ein "fremdes" Programm P2 (I4) wird mittels protokolliertem Code-Review einer **Qualitätsüberprüfung** unterzogen.

## Anforderungen

* KN-E3 muss abgeschlossen (Pass) sein.
* Niveau 4, die Anforderungen von E3 sind übernommen und werden nun **selbstständig umgesetzt**.
* Abnahme eines I4-Projekte P2 von **"fremden" Programmierern**.


## Vorgehen

1. Platzieren Sie die *kopierte* Planer-Karte vom KN-Feld E4 in ihren Planer "**In progress**".
2. Suchen Sie einen **"fremden" Programmierer**, der ihnen sein I4-Programm P2 prüfen lässt.
3. Der Code-Review sollte vom "fremden" Programmierer vorbereitet werden. (Siehe Kap. 2.1 [Vorlage](../../N2-Testing/Review_Vorlage_m319.docx))
4. Leiten Sie als "Experte" anschliessend durch den Review und prüfen Sie sein Programm anhand der Checkliste.(Siehe Kap. 2.2) <br> [Siehe "Quelltext Konvention TBZ-IT" in N3-Code_Formatting](../../N3-Code_Formatting)
5. Beschreiben Sie die Erkenntnisse und die Mängel des Programms ausführlich und lassen Sie den Abschlussbericht unterschreiben. (Siehe Kap. 3 und 4)
5. Informieren Sie die LP und geben Sie das Protokoll ab (Planer-Karte auf "**To examine**" mit Link in der Planer-Karte &#8594; Portfolio).
6. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" &#8594; 4.

### Bewertung:

Ein erfolgter Projekt-Review gibt 1 Punkt für den signierten Bericht! <br>
Unfertige Berichte werden entsprechend der Leistung prozentual teilbewertet.








