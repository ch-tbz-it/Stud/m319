![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

[TOC]

![DIY](../../x_gitressourcen/Train_D1.png)![Proof](../../x_gitressourcen/Proof.png)

# Kompetenznachweis A3


# Auftrag: (Gruppenarbeit)

Komplexe Iterationen, Funktionen und verschachtelte Kontrollstrukturen können dargestellt und interpretiert werden. (z.B. Activity- Diagram)

## Anforderungen

* Wie wird ein AD gezeichnet, wenn irgendwo aus der Mitte eine Iteration frühzeitig beendet wird?
* Wie wird eine Funktion im AD gezeichnet und interpretiert (Definition, Aufruf)
* Wie kann im AD der Programm- vom Datenfluss unterschieden werden?
* Der Ablauf von komplexen Aufgabenstellungen mit mehreren Verschachtelungen und Funktionen kann korrekt erklärt werden.
* Komplexe Funktionsweise im AD ersichtlich anhand von problemlösenden Beispielen mit Variablen und Bedingungen (<, =, >, ...)
