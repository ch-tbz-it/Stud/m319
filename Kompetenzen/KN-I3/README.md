![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

[TOC]

![DIY](../../x_gitressourcen/Train_D1.png)![Proof](../../x_gitressourcen/Proof.png)

# Kompetenznachweis I3


# Projekt P1: (Einzelarbeit)

Ein grob beschriebener Ablauf kann im Detail konkretisiert und strukturiert programmiert werden, mit Unterteilung in Methoden. Instanzvariablen, lokale Variablen und Parameter können gezielt eingesetzt werden.
Das Programm verwendet das EVA-Prinzip.


1. Platzieren Sie die *kopierte* Planer-Karte vom KN-Feld I3 in ihren Planer "**In progress**".
2. Erstellen Sie eine **Anforderungsliste *IN* der Planer-Karte** und besprechen Sie mit der Lehrperson ihr Vorhaben anhand ihrer Anforderungsliste. Das Programm muss Niveau-gerecht sein. ![Planercard](../x_gitressourcen/Planer Card Own Task.png)
3. Setzen Sie den Auftrag in *einem* Java-File um. (Einzelarbeit)
4. Platzieren Sie die entsprechende Planer-Karte im Planer auf "**To examine**". <br> Ein Link zum Programmquellcode kann in der Planerkarte gesetzt werden.
5. Informieren Sie die LP und demonstrieren Sie ihr Lösungs-Programm P1.
6. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" &#8594; 3.


## Anforderungen

* Niveau 3, d.h. alle Anforderungen von I1 und I2 sind gesteigert.
* Das Programm genügt dem **EVA-Prinzip** und ...
* ist **robust** umgesetzt,d.h. falsche Eingaben werden mit Fehlermeldung abgefangen.
* Erfüllt "Quelltext Konvention TBZ-IT". [Siehe N3 Code Formatting](../../N3-Code_Formatting)


## Ideen

* Aus Mathe, Geometrie, Physik: Dreiecks- ,Vierecks-, Geschwindigkeitssberechnungen, Pythagoras, Umrechnung von Masseinheiten, etc.
* BMI-Rechner, Alkoholspiegel-Rechner, Alter in Tagen oder Monaten
* Generatoren: Passwort, Benutzer, Adressen, Prüfcode (z.B. 6**0**8**22**4), ...
* Textmanipulation: Gross-Klein, Wörter ersetzen, Sätze rückwärts, ...
* Wortpuzzle: Wörter verstecken, Kreuzworträtsel, Wörterquadrat, Wortnetz, ...
* Verschüsselung: Text ver- und entschlüsseln mit ROT- (Cäsar-), XOR-, Vigenère-Verfahren
* Spiele (ohne KI): TicTacToe, Hangman, Mastermind, ...




