![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg) √

[TOC]

# Programmiertipps

---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:

* Allg. Programmiertipps
* Einführung in weitere komplexe Datentypen 
* Einführung in Dateihandling
* Einführung Kommandozeilenparameter

---

![Learn](../x_gitressourcen/Learn.png)

# Debugging mit bedingter Terminalausgabe

Es ist eine gängige (Anfänger-) Art beim Debuggen einzelner Variableninhalte auf das Terminal auszugeben. Nachteil ist, dass diese PrintLn-Befehle oft vergessen gehen und im finalen Programm zurückbleiben. Mit dem einfachen Trick lassen sich die `println`'s global ein- bzw. ausschalten:

`boolean dbgPrintout = true` oder `false;` steuert die Ausgabe.

`if (dbgPrintout) System.out.println("Zeit : Ort : Var = Inhalt...`  muss hinzugefügt werden. 


Beispiel:

```java
public class DebugOnOff {
    
    boolean dbgPrintout = true; // set debug  print out globally

    public static void main(String[] args ) {

        int var = 2;

        if (dbgPrintout) System.out.println("Im Main: var=" + var);

        // Calls the funtion "func"
        func();

        if (dbgPrintout) System.out.println("Im Main: var=" + var);
    }

    // function which runs under main
    public static void func() {

        int var = 3;
        
        if (dbgPrintout) System.out.println("In Func: var=" + var);
    }

}
```

**Für Profis**: Man könnte hier auch die Hilfsklasse [`Logger`](https://docs.oracle.com/en/java/javase/16/docs/api/java.logging/java/util/logging/Logger.html) verwenden: [Beispiel](https://stackoverflow.com/questions/15758685/how-to-write-logs-in-text-file-when-using-java-util-logging-logger/15758768)

-

# Gebrochene Zahlen

**Achtung** beim Vergleichen von `float`- und `double`-Zahlen: 

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap. 3.6](../Knowledge_Library/Java_Programmieren.pdf)

-
# Funktionen robust machen

Bei Funktionen mit Parameterübergabe Wächter einbauen: 

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap. 5.6](../Knowledge_Library/Java_Programmieren.pdf)

-

# Merker (Flags)

* Es ist oft ratsam, logische Ausdrücke in einer Variable (vor) zu verarbeiten: Der Quellcode wird sprechender und die Bedingung lässt sich leichter debuggen!


```java
	 //Ohne Vorverarbeitung:
	 if ( string == null || string.length() == 0 )
```

wird zu:
	 
```java	 
	 //Mit Vorverarbeitung und Benennung: 
	 boolean hasNoText = (string == null || string.length() == 0)
	 if ( hasNoText ) { ... }
```

* Schleifen mit einem Flag (Flagge, Merker) zu kontrollieren: Der Programmablauf lässt sich so an beliebigen Stellen steuern!

```java
package ch.tbz;
import static ch.tbz.lib.Input.*;

public class FlagControlledLoop {

    public static void main (String[] args){
        char letter;
        String vowels="aeiouAEIOU", enter;

		  // Flags initialisieren
        boolean wiederholen=true; // Flag kontrolliert äussere while-Schleife
        boolean istSelbstlaut=false; // Flag kontrolliert innere for-Schleife

		 // Äussere Schleife 
        while (wiederholen){  // Wiederholt bis Flag wiederholen auf false gesetzt wird ....
            enter = inputString("Gib einen Buchstaben ein: ");

			  // Innere Schleife zählt durch und wird bei einem Treffer frühzeitig abgebrochen:
            for(int i = 0; i<vowels.length(); i++){
                letter = vowels.charAt(i);
                
                // Innere-Schleife sucht jeden Selbstlaut ab
                if(letter== enter.charAt(0)){
                    istSelbstlaut = true;  // Frühzeitiger Ausstieg aus innerer for-Schleife
                    break;
                }
            }
            if (istSelbstlaut){ // erkennt frühzeitigen Ausstieg aus innerer Schleife
                System.out.println("Ist ein Selbstlaut!");
                wiederholen=false;  // Erzwingt nun Ausstieg aus der äusseren while-Schleife
            }
            else{
                System.out.println("Ist kein Selbstlaut! Versuchen Sie es ncohmals ...");
            }
        }
    }
}
```

[Weitere Beispiele](https://www.delftstack.com/de/howto/java/break-out-of-nested-loops-in-java/)

-

# Bessere Logik

Bei der Wahl der Bedingungen kann optimiert werden:
[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap. 3.7, 3.8](../Knowledge_Library/Java_Programmieren.pdf)
-

# Bitmaskierung (Mengen)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Anhang A.16](../Knowledge_Library/Java_Programmieren.pdf)

-


# Enums (Aufzählung)

Verwenden von sprechenden Konstanten anstelle von Zahlen:

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Anhang A.15](../Knowledge_Library/Java_Programmieren.pdf)

[W3S Enum](https://www.w3schools.com/java/java_enums.asp)

**Für Profis:** [Buch Java ist auch eine Insel: enums](https://openbook.rheinwerk-verlag.de/javainsel/09_006.html#u9.6)

-
# Dateihandling 
In den Unterlagen wird die "alte" IO-Biblothek vorgestellt. Verwenden Sie in ihren Programmen vorzugsweise die "neue" **NIO**-Biblothek:

![Files](x_gitressourcen/Files.png)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Anhang A.20](../Knowledge_Library/Java_Programmieren.pdf)

[W3S Files](https://www.w3schools.com/java/java_files.asp)

[W3S Create Files](https://www.w3schools.com/java/java_files_create.asp)

[W3S Read Files](https://www.w3schools.com/java/java_files_read.asp)

[W3S Delete Files](https://www.w3schools.com/java/java_files_delete.asp)

-

# Date and Time 

![Localdate](x_gitressourcen/LocalDate.jpg)

**Look up input Library of the template:** [inputDateTime()]()

[W3S Date and Time](https://www.w3schools.com/java/java_date.asp)

[Parse Date or Time](https://www.geeksforgeeks.org/localtime-parse-method-in-java-with-examples/)
[API DateTimeFormatter](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html)

**Hinweis**: Legacy Klassen nicht mehr verwenden: (auch nicht SimpleDateFormat)
![Tabelle](./x_gitressourcen/DateTimeTabel.png)

**For Profis:** [Java ist auch eine Insel: Datum & Uhrzeit](http://dev.usw.at/manual/java/javainsel/javainsel_10_007.htm#Rxx365java10007040003451F014100)

-

# Collection Framework

Neben den ArrayLists gibt es noch weitere Sammlungen (komplexe Datentypen): 

![Collections](x_gitressourcen/Collections.png)

![Maps](x_gitressourcen/Maps.png)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Anhang A.21](../Knowledge_Library/Java_Programmieren.pdf)

-

# Kommandozeileneingabe

![Args](x_gitressourcen/Args.png)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Anhang A.23](../Knowledge_Library/Java_Programmieren.pdf)

Buch.png