# Marathonlauf - Rangliste

Sie programmieren die Rangliste eines (z.B.) Marathon-Laufes. Es gibt eine Menge an Teilnehmer. Es gibt Frauen und Männer in unterschiedlichen Altersklassen. Die Daten kommen alle in Dateien vor. Es gibt als z.B. folgende Dateien:

**Startliste**

    Startnummer;Name;Sex;Jahrgang;Kategorie
    1;Anna;F;2002;F20
    2;Berkan;M;2004;M20
    3;Chris;M;2003;M20
    4;David;M;1994;M30
    5;Emil;M;1992;M30
    6;Franz;M;1993;M30
    7;Gabi;F;2001;F20
    8;Hilde;F;1971;F50
    9;Ida;F;1974;F50
    10;Josef;M;1990;M30
    11;Kathi;F;1973;F50
    12;Lenny;M;2003;M20
    13;Marina;F;2001;F20
    14;Nora;F;2002;F20
    15;Otto;M;1993;M30
    16;Paul;M;1993;M30
    17;Rudi;M;1990;M30
    18;Stefan;M;2002;M20
    19;Taulant;M;2003;M20
    20;Urim;M;2001;M20
    21;Verena;F;1974;F40
    22;Walti;M;1992;M30
    23;Xherdan;M;2004;M20
    24;Yvonne;F;2003;F20
    25;Zoltan;M;1994;M30

Aus einem Sensor (Fussmatte, Die Läufer tragen ein Chip am Fuss) kommen folgende Daten als Dateien heraus:

**Zwischenresultate**

    Startnummer;Timestamp
    12;2024-04-09_15:24:234
    18;2024-04-09_15:24:563
    1;2024-04-09_15:25:003
    (geben Sie hier weitere Zwischenzeiten aller Athleten ein)


**Schlussresultate**

    Startnummer;Timestamp
    18;2024-04-09_17:14:423
    1;2024-04-09_17:14:032
    12;2024-04-09_17:16:543
    (geben Sie hier weitere Schlusszeiten aller Athleten ein)



Der Start war um 

    2024-04-09_13:01:201


- Machen Sie eine Funktion (Methode), mit der man manuell entweder Schlusszeiten oder auch Zwischenzeiten von Athleten erfassen kann.
- Erstellen Sie nun die Rangliste "over all" 
- Erstellen Sie die Ranglisten pro Kategorie.

