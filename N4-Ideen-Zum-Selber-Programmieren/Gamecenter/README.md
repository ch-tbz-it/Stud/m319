# Gamecenter


In Zeiten, wo Sie im Internet (ChatGPT) schon viel Programmcode finden, muss hier nicht mehr erklärt werden, wie man einfache textbasiete Spiele wie **"Vier gewinnt"**, **"Hang man"**, **"Tic-Tac-Toe"**, **"Zahl erraten"**, **"Wort erraten mit Buchstabenhilfe"** usw. programmiert. 

Erstellen Sie für jedes Spiel eine Klasse (diese Klassen haben alle **keine** main-Methode!! Sie haben nur Bedienungs- und Aufrufmethoden) und machen Sie dann eine zentrale Klasse (Starter-Klasse mit einer main-Methode), die den Benutzer auffordert, seinen Namen einzugeben, ein Spiel zu wählen und ihn dann spielen zu lassen. Für den Benutzer soll dann protokolliert werden, ob und wann er gewonnen hat und wann nicht. Implementieren Sie mindestens 3 Spiele, die mehrere Benutzer spielen können. 

Diese Resultate sollten Sie dann in entsprechenden Dateien gespeichert werden. Sehen Sie vor, dass mehrere Spieler spielen können. 

Machen Sie dann eine Rangliste (top score) für einzelne Spiele und auch für alle Spiele.
