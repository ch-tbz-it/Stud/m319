![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)√

[TOC]
# Ideen zum selber Programmieren

Mögliche Aufgabenstellungen für `Niveau 4`.


Hier müssen Sie **alleine** ein kleines Programm machen, 
das **eine höhere Komplexität** als in den Übungen in 
I3 (und I2), bzw. D3 und A3 hatten. 
Dieses Mal kommen aber alle Kompetenzbereiche 
A4, D4, I4 und E4 (A=Aktivitätendiagramm, D=Datenstrukturen, 
I=Implementation, E=Qualität/Tests/Review) zusammen.

Als Steigerung für "**D**" könnten die Datenstrukturen **mit Dateien** ergänzt werden.

Als Steigerung bei der "**I**" Kompetenz ist, dass mit mehreren (eigenen) Klassen und deren Methoden (Funktionen) gearbeitet wird.

**Sprechen Sie mit Ihrem Lehrer die Aufgabe ab.** 




## Vorschlag A - [Gamecenter-Rangliste/Scoreboard](Gamecenter)

Spielen ist die *eine* Sache. Aber das alleine genügt nicht. Wir wollen hier die Spiele aufrufen und dann die High-Score der gespielten Spiele für alle Spieler sehen.

## Vorschlag B - [Sponsorenlauf](Sponsorenlauf)

In vielen Vereinen werden Sponsoren-Läufe organisiert, um Geld für den Verein zu schaffen und um die Fitness der Athleten (Läufer, Schwimmer, ...) zu fördern. Hier sollen die Athleten von Geldgebern für jede absolvierte Runde einen Geldbetrag suchen. Dann wird über einen Zeitabschnitt, zB. 30 min, so viele Runden wie möglich gelaufen. Da kommt schon etwas Geld zusammen. Sie erstellen hier Ranglisten über die schnellsten Rundenzeiten, die schnellsten Durchschnittszeiten und wer am meisten Geld eingelaufen hat. 


## Vorschlag C - [Marathonlauf-Rangliste](Marathonlauf)

In einer Laufveranstaltung nehmen Läuferinnen und Läufer in verschiedenen Alterskategorien teil. Es werden die Zwischenzeiten irgendwo in der Mitte und die Schlusszeiten in einer Datei registriert. Über diese registrierten Zeiten können dann Ranglisten erstellt werden. Für jede Alterskategorie werden Ranglisten erstellt. Es wird auch je eine Rangliste für Frauen und für Männer "over all" erstellt. 


## Vorschlag D - [Passwortmanager](Passwortmanager)

Sie können ein Passwort bestimmen. Dyas ist ein einfaches Progrämmchen. Jetzt geht es darum, Ihre Passwörter zu speichern. Und zwar: Für jede Applikation gibt es ein Ort (=Applikation), eine Benutzerkennung und das Passwort. Man soll jetzt alle Passwörter als "Hash" speichern und man soll alle Passwörter wieder hervorbringen können.

## Vorschlag E - [Hotelbuchungssystem](Hotelbuchungssystem)

Ein Buchungssystem (Hotel-, Kino-, Konzert- oder Theaterbuchungen) trägt die Buchungen in eine Datei ein. Man muss Buchungen machen (erstellen) können und man soll Buchungen löschen können. Die Buchungen sollen aus der Datei im Kalender (Tage der Buchung) angezeigt werden. Man soll eine Buchungsliste erhalten können als Betreiber der Lokalität, und die Personen, die eine Buchung bekommen haben, sollen ihr "Ticket" auch angezeigt bekommen.


## Vorschlag F - [Pendenzenliste, ToDo-Liste](Pendenzenliste)

Pendenzen sollen in ein Kalender (Tagesablauf) eingetragen werden. Pendenzen sind in einer Datei zu speichern und haben eine ID, ein Titel, ein Detailtext und ein Datum mit Startzeit plus eine Dauer. Man soll jetzt Pendenzen eröffnen können, löschen können und verschieben können. Ebenso soll jede Pendenz ein Status wie "geplant", "in Arbeit", "abgeschlossen" haben und man soll die Pendenzen entsprechend vom einen zum anderen Status wechseln können.

<br>

<hr>