# Pendenzenliste - ToDo-List


Das System soll können:

- Pendenz erfassen
- Pendenz löschen
- Pendenz ändern
- Pendenz markieren als
    - geplant
    - inarbeit
    - erledigt
- Alle Pendenzen auflisten



Überlegen Sie sich die Datenstruktur einer Pendenz und machen Sie eine class `'Pendenz.java'`, die diese Daten enthält.


Alle Pendenzen sollen in eine Datei abgelegt werden (csv-Datei). Die Auflistung soll ab dieser Datei erfolgen.