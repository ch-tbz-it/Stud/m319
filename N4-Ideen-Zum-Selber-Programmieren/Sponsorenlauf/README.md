# Sponsorenlauf

## Aufgabe
Einen Zeitstopper für die jährliche Sammelaktion des FC Mutschellens "Runden Rennen" mit einer Gesamtdauer von 30 Minuten.

## Programmier-Ansätze:

    Startzeit Runde, Schlusszeit Runde, Durchschnittsdauer pro Runde, Anzahl Runden, Mitgliedsnummer, Beitrag pro Runde in CHF

- Datei 1: TIMESTAMPS (Runden-Durchgangszeiten)
- Datei 2: Name, Mitgliedsnummer
- Datei 3: Mitgliedsnummer, current_Beitragssumme


## Zu erstellen ist:

Eine Rangliste (& Belohnung für die) 
- Top 3 der höchsten Beitragssummen 
- Schnellste Durchschnittsdauer