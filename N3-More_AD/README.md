![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)√

[TOC]

# Mehr über Aktivitätsdiagramme (AD)

---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:
* Weitere Symbole und der Objektfluss werden erklärt.
* Beispiele weiterer ADs.
* Unterschiedliche Arten, ein AD zu formulieren.

---

![Learn](../x_gitressourcen/Learn.png)

# Weitere Symbole zum AD:

![Video:](../x_gitressourcen/Video.png) Symbole erklärt: 7 min (2:00 - 8:30)
[![Erklärung](https://img.youtube.com/vi/XFTAIj2N2Lc/0.jpg)](https://www.youtube.com/watch?v=XFTAIj2N2Lc)

## Bedeutung der Symbole

| Symbol | Bedeutung | Anwendung |
|:---:|---|---|
|![Fork](./x_gitressourcen/Rahmen.png)|Diagrammrahmen mit Bezeichner|Für ein Programm, Unterprogramm oder eine Funktion |
|![Objekt](./x_gitressourcen/Objekt.png)|Objekt|Bezeichnet relevante Daten. Bezeichnet die Daten im  Datenfluss. |
|![Fluss](../N1-UML_Activity_Diagram/x_gitressourcen/Flow.png)|Objektfluss|Pfeile zeigen *auch* den Objektfluss - je nach verbundenen Symbolen:|
|![In](./x_gitressourcen/In.png)![Out](./x_gitressourcen/Out.png)|Eingabe- oder Ausgabeobjekt aus einer Aktion |Daten, die von der Aktion eingelesen oder weitergegeben werden.|
|![Trans](./x_gitressourcen/Trans.png)| Transition  | Ein Objekt (Daten) wird überführt, übermittelt. |
|![Verzeigung](./x_gitressourcen/NeuObjekt.png)| Objekt zu Aktion | Aktion erstellt oder verändert Objekt, z.B. neue Datenstruktur wird bereitgestellt. |
|![Call](./x_gitressourcen/ObjektInput.png )|Objekt zu Aktion | Aktion wird durch Daten angestossen |
|![Join](./x_gitressourcen/Strukturelement.png)| Bereichsrahmen | Eine Kontrollstruktur wird hervorgehoben, z.B. Iteration |
|![Break](./x_gitressourcen/Break.png)| BREAK: Abruch  | Eine Kontrollstruktur wird abgebrochen, z.B. while .. break |
|![Continue](./x_gitressourcen/Continue.png)| CONTINUE: <br> Zurück zum Anfang der Kontrollstruktur | Eine Kontrollstruktur wird frühzeitig wiederholt, z.B. while .. continue |
|![Data](./x_gitressourcen/Unterbruch.png)| Ausnahme | Ein neuer Ablauf wird eingeleitet, hier durch Empfang einer Nachricht (Event, Exception).|
|![Data](./x_gitressourcen/End.png)| Verlassen | Ein Ablauf wird vorzeitig verlassen (Exit, Return).|
|![Data](./x_gitressourcen/Timer.png)| Timer | Zeit wird abgewartet (Warten, Dauer, ... ) |

### Hinweis: Kurzschreibweise einer Aktion mit Entscheidung
![Kurzschreibweise](./x_gitressourcen/Kurzschreibweise.png)
Erklärung: Die Entscheidung erfolgt in der Aktion und wird nicht separat visualisiert.

---

![Train](../x_gitressourcen/Train_R1.png)

## Objektfluss

![ToDo](../x_gitressourcen/ToDo.png) To Do:

1. **Interpretieren Sie folgende drei ADs mit den "neuen" Symbolen:** Beachten Sie die Symbole (Objekt oder Aktion) bei den Pfeilen! <br><br>
	a) **Allgemein** formuliert: <br> ![Kurzschreibweise](./x_gitressourcen/AD_Abstraktion.png) <br>
	b) Programm **Quadrieren** <br> ![Kurzschreibweise](./x_gitressourcen/AD_Quadrieren.png) <br>
	c) Programm **Harasse** <br> ![Kurzschreibweise](./x_gitressourcen/AD_Harasse.png)

---

![Learn](../x_gitressourcen/Learn.png)


ADs können unterschiedlich ausformuliert werden. Auf der **Metabene** oder mit konkreten Angaben zum **Quelltext**. Entsprechend sind die ADs für unterschiedliche Zwecke gedacht:

# Beispiel AD anhand der Anforderungsliste (Design)

Anforderungsliste "**Städteraten**":

1. Wir wollen eine ArrayList für ein kleines Gedächtnisspiel nutzen. 
2. Der Anwender gibt Städte für eine Route vor, die sich das Programm in einer Liste merkt. 
3. Nach der Eingabe eines neuen Ziels auf der Route soll der Anwender alle Städte in der richtigen Reihenfolge wiedergeben. 
4. Hat er das geschafft, kommt eine neue Stadt hinzu. 
5. Im Prinzip ist das Spiel unendlich, doch da sich kein Mensch unendlich viele Städte in der Reihenfolge merken kann, wird es zu einer Falscheingabe kommen, was das Programm beendet.

![Kurzschreibweise](./x_gitressourcen/AD_Staedte1.png)

> Die Beschreibung der Aktionen ist oft auf der **Metaebene** formuliert!
> Dient auch als Alternative zur Anforderungsliste. (Siehe [N2-Task_Description](../N2-Task_Description))


# Beispiel AD anhand des Quelltextes (Systemdoku)

Programm "**Städteraten**":

```java
public class HowDoesYourRouteLooksLike {

  public static void main( String[] args ) {
    ArrayList<String> cities = new ArrayList<String>();

    while ( true ) {
      
      String newCity = inputString( "Welche neue Stadt kommt hinzu?" );      
      cities.add( newCity );

      System.out.printf( "Wie sieht die gesamte Route aus? (Tipp: %d %s)%n",
                         cities.size(),
                         cities.size() == 1 ? "Stadt" : "Städte" );

      for ( String city : cities ) {
        String guess = inputString("");
        if ( ! city.equalsIgnoreCase( guess ) ) {
          System.out.printf( "%s ist nicht richtig, %s wäre korrekt. Schade!%n",
                             guess, city );
          return;
        }
      }
      System.out.println( "Prima, alle Städte in der richtigen Reihenfolge! \n" );
    }
  }
}
```

![Kurzschreibweise](./x_gitressourcen/AD_Staedte2.png)

> Die Beschreibung der Aktionen beinhaltet oft **Quelltext**! 
> Dient der **Klärung eines komplexen Ablaufs**!


---

![CP](../x_gitressourcen/CP.png)

# CheckPoint

* Alle neuen Symbole und deren Bedeutung sind bekannt und können angewendet werden.
* Objektfluss kann im AD interpretiert werden.
* Eine Anforderungsliste kann in ein AD überführt werden.
* Ein Programm (oder Teil davon) kann durch ein AD visualisiert werden.


