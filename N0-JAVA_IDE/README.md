![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)√

[TOC]

# JAVA IDE (& SDK)


---

![Lernziel](../x_gitressourcen/LZ.png)

### Lernziel:
* Überblick JAVA Plattform
* Was ist eine "IDE"
* Installation einer IDE (IntelliJ)
* Integration von GitLAB in IntelliJ

--- 

![Learn](../x_gitressourcen/Learn.png)

## Die JAVA Plattform

![Plattform](./x_gitressourcen/Plattform.jpg)

- **JDK**: JAVA Development Kit - Software Entwicklungs Kit (SDK) <br>
- **JRE**: JAVA Runtime Environment - reine Ausführungsumgebung für Java <br>
- **JVM**: JAVA Virtual Machine - Basis des Interpreters (java.exe) <br>
- **Entwicklungsprogramme (Development Tools)**: <br> CL-Tools* im **bin**-Ordner der JDK:
   **javac.exe** ist der Compiler, **java.exe** ist das ausführende Programm (Interpreter) <br>
- **IDE**: Integrierte Software Entwicklungsumgebung mit graph. Oberfläche

*) CL = Command Line

## Was ist eine IDE?

![Video:](../x_gitressourcen/Video.png)
[![Was ist eine IDE](https://img.youtube.com/vi/vUn5akOlFXQ/0.jpg)](https://www.youtube.com/watch?v=vUn5akOlFXQ)



## Welche IDE soll ich auswählen?

[Überblick und Empfehlung](https://www.educative.io/blog/best-java-ides-2021)

Abstimmung:

![](./x_gitressourcen/Abstimmung.png)

---


![Train](../x_gitressourcen/Train_R1.png)

![ToDo](../x_gitressourcen/ToDo.png) To Do:

## JAVA SDK:

Zur Ausführung von Java brauchen wir die JDK (Java Development Kit / Aktuelle Version (23 oder höher)) auf unserem Rechner:

[Download](https://www.oracle.com/java/technologies/downloads/)

Verschaffen Sie sich einen **kurzen Überblick** über die JDK auf Wikipedia:

[Überblick (Wikipedia)](https://de.wikipedia.org/wiki/Java_Development_Kit)

---

![ToDo](../x_gitressourcen/ToDo.png) To Do:

## Installation der IDE "IntelliJ IDEA":

[Download-Page](https://www.jetbrains.com/de-de/idea/download/) - Die **Community Edition** ist zu bevorzugen. <br>
Hinweis: Ultimate kann mit TBZ-E-Mail freigeschaltet werden.

### IntelliJ für WIN11: 

![Video:](../x_gitressourcen/Video.png)
[![IntelliJ](https://img.youtube.com/vi/j6mLD6e75Go/0.jpg)](https://www.youtube.com/watch?v=j6mLD6e75Go)

> **Anm**.: Vergesst nicht in der Systemeinstellung, den "PATH"- und "JAVA_HOME"-Variablen den JDK-Pfad anzuhängen! Siehe im Video ab Position 5:38!

### IntelliJ für MAC:

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/zCkhAVhuILs/0.jpg)](https://www.youtube.com/watch?v=zCkhAVhuILs)


[IntelliJ Helpseite](https://www.jetbrains.com/help/idea/discover-intellij-idea.html) 

### Einführung in die IDEA-Tutorials: Verschaffen Sie sich einen Überblick

![Video:](../x_gitressourcen/Video.png)
[![Tutorial](https://img.youtube.com/vi/vsUx-kod2O4/0.jpg))](https://www.youtube.com/watch?v=vsUx-kod2O4)

---

![ToDo](../x_gitressourcen/ToDo.png) **To Do nur für APIs und Profis**:

## Einbindung GitLAB/GitHUB in IntelliJ: 
Kann auch zuhause gemacht werden!

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/uUzRMOCBorg/0.jpg
)](https://www.youtube.com/watch?v=uUzRMOCBorg)

---

![CP](../x_gitressourcen/CP.png)

# CheckPoint
* Wozu wird eine **IDE verwendet**?
* Welches ist die **neueste Version** der Java JDK?
* Die JDK besteht aus einigen Programmen. Nenne die **zwei wichtigsten Programme**, um ein eigenes Programm entwickeln zu können.
* Auf welchen **Plattformen** läuft JAVA?
* **Erstes Programm** in JAVA zum Laufen gebracht. (&rarr; Einführung in das IDEA-Tutorial: "Hello IDEA" Min 2.00)
* Nur für APIs und Profis: Mein erstes JAVA Programm (Projekt) in mein Git-Repo (Server) hochgeladen.


