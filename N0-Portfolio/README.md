![TBZ Logo](../x_gitressourcen/tbz_logo.png)√

[TOC]

# Lern-Portfolio
---

![Lernziel](../x_gitressourcen/LZ.png)

### Lernziel:
* Zweck, 
* Wahl und 
* Installation eines **persönlichen, digitalen Lern-Portfolios**
* Lernstrategie anhand des Lernzyklus
* Einsatz von KI.

-- 

## Was ist eine Portfolio?
![ePortfolio](./x_gitressourcen/eportfolioWords.jpg)

Der Begriff "Portfolio" wird vielfältig benutzt:

* Sammlung von (eigenen) Kunstwerken
* Sammelmappe mit Bewerbungsunterlagen
* Sammlung von Wertpapieren (z.B. Aktien)
* Sammlung aller Produkte einer (Hersteller-) Firma
* Sammlung (Ablage) mit Belegen, Dokumenten, Resultaten und Produkten ➞ **Nachweis-Portfolio** 
* Sammlung von Lern-Ressourcen und -Notizen ➞ **Lern-Portfolio**

![Übersicht](./x_gitressourcen/Lern-Portfolio.png)

## Wozu ein Lern-Portfolio?

Beim SOL werden Sie viele Themen, Anwendungen und Erfahrungen kennenlernen und entwickeln. Diese "Lernelemente" gehen verloren, wenn Sie diese nicht "verarbeiten". (Beim *reinen Lesen* hat das Kurzzeitgedächnis eine Latenzzeit von wenigen Minuten!)

Durch das geeignete Übertragen ( ➞ Skizzen, Notizen, Mindmap, Vernetzung, usw.) in ihr Lernportfolio "verarbeiten" Sie die Lernelemente und übertragen diese so ins Langzeitgedächnis. "Später" (im Berufsleben) können Sie ihre Portfolios wieder durchkämmen, die Lernelemente nachlesen und durch Repetition auffrischen. Oder auch als Belege in Ihr CV übertragen!

Die beste Wirkung hat ein Lernportfolio, wenn Sie die Lernelemente vernetzen und auch im Berufsalltag weiterentwickeln.

## Der Selbst-Lern-Zyklus

Im Unterricht sind Sie als Lernender **EntdeckerIn** und **OrganisatorIn** Ihres eigenen Lernprozesses. Folgendes Diagramm gibt eine empfohlene Lernstrategie vor:
![](./x_gitressourcen/Selbst-Lern-Zyklus_kl.png)  

![](./x_gitressourcen/Organize.png) 

**ORGANIZE:** Alle folgenden Schritte des Lernzykluses sind entsprechend zu organisieren! Z.Bsp.:

- Learnmap mit Avatar oder Dots
- KANBAN Planer
- Anmelden Kompetenznachweis

![](./x_gitressourcen/Learn.png) 

**LEARN:**

- Bestimmen Sie das nächste zu lernende Thema. (➞ Learnmap, Kompetenzraster) 
- Verschaffen Sie sich einen Überblick über alle dazugehörigen Ressourcen. (➞ GitLAB)
- Arbeiten Sie sich ins Thema ein - durch Lesen und Schauen der Lernvideos (➞ Stream)
- Nachmachen: Soweit möglich testen Sie die vorgestellten Praktiken selber aus.
- Notieren oder kopieren Sie die neuen Erkenntnisse ins *Lern-Portfolio*.

![](./x_gitressourcen/Train_R1.png)

**TRAIN & REFLECT:** Wenden Sie aktive Lernmethoden an, um das Aufgenommene zu verarbeiten:

- Vernetzen Sie die Notizen in Ihrem *Lern-Portfolio*. (➞ Umstellen, Gruppieren, Pfeile, ...) 
- Erstellen Sie Fragen zum Thema anhand der Einträge in Ihrem *Lern-Portfolios*. (➞ Active Recall)
- Beantworten Sie gegenseitig Ihre Fragen im Tandem. (➞ Lernlücken? Neue Erkenntnisse?)

![](./x_gitressourcen/Train_D1.png) 

**TRAIN - DoItYourself:** 

- Versuchen Sie anhand des Erlernten eigene Beispiele, z.B. aus ihrer Berufspraxis, umzusetzen. Variieren, ergänzen, kombinieren Sie ... seien Sie kreativ! 
- Evtl. finden Sie nun konkrete Probleme, welche Sie nun selbstständig lösen sollten. 
- Tauschen Sie im Tandem oder mit anderen Lerngruppen über ihre Erkenntnisse aus und arbeiten Sie gemeinsam an aufkommenden Hindernissen. (➞ 4K).
- Gehen Sie die offenen Fragen der Checkpoints durch und beantworten Sie diese im *Lern-Portfolio*.

![](./x_gitressourcen/Proof.png) 

**PROOF:** Nun schliessen Sie eine "Lernschleife" mit dem Kompetenznachweis ab. 

- Legen Sie dazu alle nötigen Belege und Produkte im *Nachweis-Portfolio* zur Einsicht ab. 
- Die Lehrperson wird ihre *Kompetenz* als Experte *sicherstellen*: 
  - durch Sichtung ihres Lernprodukts
  - durch Präsentation ihrer EIgenöeistung, ihrer Kompetenz
  - durch Fachgespräch oder Reflektion
  - durch ihr Feedback innerhalb des Tandems
  - ...
- Bei *ungenügendem Nachweis* wird der Experte Ergänzungen einfordern.
- Evtl. ergänzen Sie Ihr *Lern-Portfolio* mit den beim Nachweis erworbenen Erkenntnissen.
- Besprechen Sie mit der Lehrperson die nächsten geplanten Schritte. (➞ Kompetenzraster)


## Einsatz einer KI als Tutor

Grundsätzlich ist nichts gegen den Einsatz einer KI als Tutor (Coach) einzuwenden. Hier die Vorteile, die eine KI im Bildungskontext leisten kann: 

> *DeepSeek Jan-25 / Prompt: "In welcher Phase und wie würdest du als Lernender eine KI einsetzen?"* <br> 
> KI ist ein flexibles und mächtiges Werkzeug, das in allen Phasen des Selbstlernzyklus eingesetzt werden kann. Als Lernender würde ich KI nutzen, um meinen Lernprozess zu **personalisieren**, zu **beschleunigen** und **reflektierter** zu gestalten. Gleichzeitig ist es wichtig, **kritisch mit den Ergebnissen** umzugehen und die KI als unterstützendes Werkzeug zu sehen, **nicht als Ersatz für eigenständiges Denken und Lernen.** <br> 

![KI-Tutor](x_gitressourcen/KI-Tutor.png)

Als Lernender sollte man den Grundsatz **"Ich will es selber wissen und können!"** haben! Die KI sollte deshalb _nur_ als Tutor eingesetzt werden und nicht als Lösungsmaschine. Geben Sie also nicht die Aufgabenstellung als Prompt ein, sondern lassen Sie sich von der KI in ihrem Denk- und Lösungsprozess unterstützen! Z.B. _Prompt: "Zeige mir alle Möglichkeiten eine fussgesteuerte Schleife zu implementieren, die mit einer Merker-Variable abgebrochen werden kann!"_
Um beim Prompten die besten Resultate zu erhalten, arbeiten Sie mit den korrekten Fachbegriffen, die sie vorher erlernen. Auch können Sie den KI-Output am besten verifizieren, wenn Sie bereits Grundwissen haben.

Ihre **Nachweise bzw. Lernprodukte** sollten also **selber getippt** sein, damit ein Lernprozess stattfinden kann! KI-Ausgaben sollten **gelesen, verstanden, verifiziert und kommentiert** sein. **Quelle** nicht vergessen anzugeben, bei KI: Modell, Datum, Prompt!

### Die KI als Tutor engagieren

Setzen Sie zu Beginn ihrer Lernaktivität folgenden **Prompt** im KI-Chatbot ab. Dieser Prompt engagiert die KI als Tutor mit einem Fokus auf lernprozessrelevante Unterstützung!

**Setup-Prompt:**

```prompt
Sei mein persönlicher Tutor für das Thema "Programmieren mit Java" auf dem Niveau der Schweizer Berufsschule für Informatik. 
Deine Aufgabe ist es, mir beim Verständnis der Grundlagen und Konzepte zu helfen, ohne direkt Lösungen für meine Aufgaben zu liefern. 
Führe mich stattdessen durch den Lernprozess, indem du mir zeigst, wie ich Probleme selbstständig angehen kann. 

Halte dich dabei an den Selbstlernzyklus, der in der folgenden Website erklärt wird: <https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-Portfolio> 


Strukturiere deine Antworten immer in die folgenden vier Kategorien: 

1) Grundlagen: Erkläre mir kurz und verständlich die Grundlagen, die für die Frage relevant sind. 
2) Konzeptverständnis: Hilf mir, das Prinzip oder die Logik hinter der Frage zu verstehen. 
3) Nächster Schritt: Zeige mir, wie ich den nächsten Schritt in meinem Lernprozess machen kann, um zur Lösung zu gelangen. 
4) Nutzung Lernportfolio: Gib mir einen Vorschlag, wie ich dazu einen nützlichen Eintrag im Lernportfolio machen kann.

Benutze immer nur die Website <https://gitlab.com/ch-tbz-it/Stud/m319> mit den Unterordnern, um den Kontext des Themas zu erfassen. Es sollen noch keine eigenen Objekte und deren Methoden verwendet werden.

Bitte halte dich strikt an diese Struktur und das angegebene Niveau.
```


---
---

![ToDo](../x_gitressourcen/ToDo.png) To Do:

# Wahl eines Tools:

Wir wollen nun ein digitales Tool auswählen, um das *Lern-Portfolio* zu führen! Vorzugsweise entscheiden Sie sich für ein Tool, das Sie bereits kennen, favorisieren oder durch die Lehrfirma vorgegeben ist (LMS). 

Das Tool sollte es ermöglichen, einzelne Bereiche (➞ Modul, Gruppe/Einzel) zu unterhalten und zu jedem Bereich einen Link erstellen zu können mit Schreibrechten. 

Diesen Link müssen Sie **für Ihre Lehrperson freigeben und überreichen**.


## OneNote

![OneNote](./x_gitressourcen/OneNote.jpg)

Ist im TBZ-Officepaket enthalten. Transfer auf andere Konten möglich. Pro Modul ein Notizbuch eröffnen.

**Anm**.: Bitte nicht über Bildschirmrand hinaus Elemente platzieren ...

![Video:](../x_gitressourcen/Video.png) 12min 
[![Tutorial](https://img.youtube.com/vi/Qc-2OUCbvO0/0.jpg)](https://www.youtube.com/watch?v=Qc-2OUCbvO0)


## Evernote

![Evernote](./x_gitressourcen/EverNote.jpg)

Der Pionier und Platzhirsch bei den Notizbüchern. Gratis sollte reichen für TBZ-Gebrauch.

![Video:](../x_gitressourcen/Video.png) 12min
[![Tutorial](https://img.youtube.com/vi/iOE-XiX95cY/0.jpg)](https://www.youtube.com/watch?v=iOE-XiX95cY)

## Confluence (LMS)

![Confluence](./x_gitressourcen/Confluence.png)

Confluence ist eine Wiki Software für die Zusammenarbeit im Unternehmen. Vorwiegend wird Confluence für die Kollaboration und das Wissensmanagement innerhalb eines Unternehmens genutzt. Zusätzlich bietet Confluence durch seine Flexibilität viele weitere Einsatzbereiche. Dazu gehören zum Beispiel die Erstellung von Intranetseiten oder QM Systeme auf Basis von Confluence. Ebenso bietet Confluence umfangreiche Integrationsmöglichkeiten zu anderen Atlassian Produkten wie Jira, Fisheye, Clover, Crucible, Bamboo oder Crowd. 

## GitLAB/HUB

Ihr GitLAB/HUB kann auch als *Lern-Portfolio* benutzt werden. Macht vor allem bei den API-Lernenden Sinn, falls sie Markdown kennen und bereits Umgang mit einem GIT-System haben.

Anm: Das *Lern-Portfolio* sollte lokal bearbeitet und auch - zur Abgabe von Belegen - auf den Server übertragen werden ("add-commit-push"). 

Siehe auch:
[Portfolio](../N0-GitLAB/readme.md#Verwendung als Lernportfolio Tool)

## Miro

Das in diesem Modul bereits kennegelernte Tool lässt sich bestens für ein *Lern-Portfolio* verwenden. Vorteil ist die 

Pro Modul ein Board verwenden. Share with Comment/Edit mit Passwort.

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/eqPz4gIKJNQ/0.jpg)](https://www.youtube.com/watch?v=eqPz4gIKJNQ)

**Anm**.: Mit dem EDU Account (&#8594; beantragen) können Sie die Lehrperson auch als Mitglied einladen.

## Weitere

Sie können ein beliebig anderes Tool verwenden, wenn Sie Bereiche freigeben können ...

## LMS (Learning Management System)

Falls Sie in der Firma ein LMS verwenden und Bereiche mit Schreibrechten an die Lehrperson weitergeben können, ist das eine gute Wahl.

---

![ToDo](../x_gitressourcen/ToDo.png) Do To: (Für Modul 319)

# Link für Tandem-Partner und LP mit Schreibrechten

1. Erstellen Sie mit ihrem Tandem-Partner einen Link auf den **gemeinsamen Bereich** dieses Moduls (Notebook, Bereich, ...) und versehen Sie den Bereich mit Schreibrechten für Ihren Tendem-Partner und die Lehrperson.

2. Übertragen Sie den Link ins Miroboard im **Team Control Center** in Ihrem Team unter "Enter Link to Group-Portfolio" mit CTRL-ALT-K:<br>![GroupLink](./x_gitressourcen/LinkGroup.png)<br> Falls sie keinen Zugang zum Link finden, senden Sie ein Einladungs-Email an die Lehrperson. Sie wird den Link dann platzieren.

3. Erstellen Sie für sich einen Link auf den **Bereich für Einzelarbeit** dieses Moduls (Notebook, Bereich, ...) und versehen Sie den Bereich mit Schreibrechten für die Lehrperson.

4. Übertragen Sie den Link ins Miroboard im **Team Control Center** in Ihrem Team unter "Enter Link to Portfolio Student X" mit CTRL-ALT-K:<br>![GroupLink](./x_gitressourcen/LinkSingle.png)<br> Falls sie keinen Zugang zum Link finden, senden Sie eine Einladungs-Email an die Lehrperson. Sie wird den Link dann platzieren.

---

![CP](../x_gitressourcen/CP.png)

# CheckPoint
* Ich weiss wozu ein *Lern-Portfolio* für den Lernprozess ist und wie man damit umgeht.
* Habe ein *Lern-Portfolio* Tool ausgewählt.
* Habe zwei Bereiche freigegeben (Group / Student) und im Miroboard eingegeben (oder Einladung an LP gesendet). <br> Benennen Sie die Bereiche/Links mit **Klasse-Modul-Name(n)**
* Melden Sie die Fertigstellung und erwarten Sie eine Bestätigung der Lehrperson, ob der Zugriff funktioniert.
* **LERNPROZESS**: "Ich werde mir beim Erstellen eines jeden neuen Lernelementes überlegen, wie ich es *formulatiere*, *niederschreibe*, *kommentiere* und evtl. *vernetze*." (Nicht COPY-PASTE)


