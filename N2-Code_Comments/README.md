![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# Quellcode Kommentar

---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:
* Zweck eines Kommentars
* Kommentararten
* Einführung in JavaDoc

---

![Learn](../x_gitressourcen/Learn.png)

## Wozu wird ein Kommentar eingesetzt

* Versieht eine Quelltext mit **Metadaten**: Angaben zum Inhalt, Autor, Verion, Änderungshistory, ...
* Kommentiert den Quellcode, um weitere (Hintergrund-)**Informationen** zu geben
* &#8594; Dient der **Orientierung** (beim Einlesen)
* &#8594; Dient der **Wartung**

---

# Überschriften /* ... */ oder //

(1) **Klassen**, **Funktionen** und wichtige **Abschnitte** sollten kommentiert werden:

* Beschreibt den nachfolgenden **Inhalt** (Übersicht)
* Können mehrzeilig ```/* ..*/``` oder auch nur einzeilig ```//```sein.
* Werden **oberhalb** Abschnitt platziert
* Haben dieselbe **Einrückung**, damit die Dastellung der Struktur nicht gestört wird.

Siehe auch (3)!

# Zeilenkommentar // ...

(2) Eine Zeile Code wird kommentiert:

* Beschreibt **WOZU** der Programmcode in der Zeile so programmiert wurde.
* **Verweisst** auf eine Quellen, einen Hintergrund oder einen Ursprung.
* Wird üblicherweise am Ende der Zeile angehängt.


**Schlechtes Beispiel:**

```java
double c = Math.sqrt(Math.pow(a,2) + pow(b,2)); // Wurzel aus a^2 plus b^2
```
Hier wird nur der Code *nochmals* erklärt.

**Gutes Beispiel:**

```java
double c = Math.sqrt(Math.pow(a,2) + pow(b,2)); // Satz von Pythagoras

```
Hier wird der *Ursprung* der Formel belegt.


# JavaDoc /** ... **/

(3) Speziell formatierter Kommentar wird zur System-Dokumentation (API) des Codes verwendet! Das Tool **JavaDoc.exe** kreiert eine HTML Seite aus dem Kommentar.

* Geeingete Überschriften werden im Bezug zum Code gebracht: Klassen, Methoden, Parameter
* Auch unkommentierte Methoden werde in die HTML-Docu eingebunden (Ohne Text)
* **HTML-Tags** können eingebunden werden.

![IntelliJ Tool Menü](./x_gitressourcen/JavaDoc.png)

# Beispielcode

![Comment](./x_gitressourcen/Comment.png)

![ToDo](../x_gitressourcen/ToDo.png) To Do:
[W3S Comments](https://www.w3schools.com/java/java_comments.asp)

---

![DIY](../x_gitressourcen/Train_D1.png)

## Für Profis: Ausführliches Tutorial

![Video:](../x_gitressourcen/Video.png) 30 min
[![Tutorial](https://img.youtube.com/vi/qLA7HhoajqM/0.jpg)](https://www.youtube.com/watch?v=qLA7HhoajqM)

IntelliJ JavaDoc: >> 27min

---
![Learn](../x_gitressourcen/Learn.png)

# Javadocs tabs


Die fettgedruckten und blauen Tags werden am häufigsten von professionellen Entwicklerteams verwendet.

Javadoc Tags | | | |
---|---|---|---|
Tag | Parameter | Usage | Applies To | Since |
<b><font color="blue">\@author</font></b> | John Smith | Describes an author | Class, Interface, Enum | N/A |
\@docRoot | | Represents the relative path to the generated document's root directory from any generated page. | Class, Interface, Enum, Field, Method | N/A |
<b><font color="blue">\@version</font></b> | version | Provides software version information. | Module, Package, Class, Interface, Enum | N/A |
\@since | since-text | Describes when this functionality has first existed. | Class, Interface, Enum, Field, Method | N/A |
\@see | reference | Provides a link to other element of documentation. | Class, Interface, Enum, Field, Method | N/A |
<b><font color="blue">\@param</font></b> | name description | Describes a method parameter. | Method | N/A |
<b><font color="blue">\@return</font></b> | description | Describes the return value. | Method | N/A |
\@exception | classname description | Describes an exception that may be thrown from this method. | Method | N/A |
\@throws | classname description | Describes an exception that may be thrown from this method. | Method | N/A |
<b><font color="blue">\@deprecated</font></b> | description | Describes an outdated method. | Class, Interface, Enum, Field, Method | N/A |
{@inheritDoc} | | Copies the description from the overridden method. | Overriding Method | 1.4.0 |
\@link | reference | Link to other symbol. | Class, Interface, Enum, Field, Method | N/A |
\@linkplain | reference | Identical to {\@link}, except the link's label is displayed in plain text than code font. | Class, Interface, Enum, Field, Method | N/A |
\@value | #STATIC_FIELD | Return the value of a static field. | Static Field | 1.4.0 |
{\@code} | literal | Formats literal text in the code font. It is equivalent to <code>{@literal}</code>. | Class, Interface, Enum, Field, Method | 1.5.0 |
{\@literal} | literal | Denotes literal text. The enclosed text is interpreted as not containing HTML markup or nested javadoc tags. | Class, Interface, Enum, Field, Method | 1.5.0 |
\@serial | literal | Used in the doc comment for a default serializable field. | Field | N/A |
\@serialData | literal | Documents the data written by the writeObject( ) or writeExternal( ) methods. | Field, Method | N/A |
\@serialField | literal | Documents an ObjectStreamField component. | Field | N/A |



source: https://en.wikipedia.org/wiki/Javadoc


---
![CP](../x_gitressourcen/CP.png)
# Checkpoint
* Kenne die drei Kommentararten
* Weiss wann welche Art einzusetzen ist
* *Für Profis: Kann ein JavaDoc HTML-Seite erstellen*


