![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![Library](./x_gitressourcen/Library.jpg)√

[TOC]

# Knowlege Library

Hier finden Sie alle referenzierten, allgemeinen Unterlagen und mehr ...

**Viel Vergnügen beim Lernen!**

## PDFs:
* [![Buch](../x_gitressourcen/Buch.png) PDF-Unterlagen (PDF herunterladen!)](./Java_Programmieren.pdf) ![Buch](./x_gitressourcen/JP.png)<br>
  [Programmieraufgabensammlung zu den PDF-Unterlagen](https://www.programmieraufgaben.ch/) <br> <br>
* [Referenz zu Java und AD](./Referenz_Java_UML.pdf)
* [IntelliJIDEA ReferenceCard](IntelliJIDEA_ReferenceCard.pdf)

## TBZ VIDEOS
[TBZ Streams Video Liste m319 JAVA](https://tbzedu-my.sharepoint.com/:l:/g/personal/michael_kellenberger_tbz_ch/FFkrpU55I79Mkl1YeelW41gBEsvG4E05UZEanxxztCg4HQ?e=bNFaHG)

## Poster:
* [Poster Konzept Variablen](../N1-Variables_Constants/ProgBASICS Variablen V1.3.png)
* [Poster "Kontrollstrukturen: AD"](../N1-Flow_Control/Kontrollstrukturen_AD.png)
* [Poster "UML V2.5"](./UML2.5(PlakatA0).png)
* [Poster "Der Softwareentwicklungsprozess"](./SW-Entwicklungsprozess.png)

## Links
* [JAVA JDK 20](https://docs.oracle.com/en/java/javase/20/index.html)
* [RedHat Open JDK](https://developers.redhat.com/products/openjdk/overview)
* [W3School Java Tutorials](https://www.w3schools.com/java/java_break.asp)
* [Java for Beginners](https://javabeginners.de/index.php) 
* [Java ist eine Insel (ausführliche Doku)](https://openbook.rheinwerk-verlag.de/javainsel/01_001.html#u1) ![Insel](./x_gitressourcen/Java_Insel_Buch.png)
* [Java advanced tutorials](http://www.java2s.com/)
* [Java Referenz](http://www.java2s.com/ref/java/java.html)
* 
* [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html#s3.3-import-statements)

## Online Java container 
* [Microsoft vscode java container](https://github.com/microsoft/vscode-remote-try-java)

## Books 

James T. (2014)

Guide to Java a concise introduction to programming 

Main Location: ZB Zürich
Secondary Location: 2.UG (frei zugänglich)
Call Number: HV 4699

note: This book is available at the Zürich central library. It is written for beginners and provides an easy entry into the topics covered in m319 such as: what is a Java program, what is a compiler,  what are data types, variables, classes etc. 

Of particular interest for m319, check these chapters:
* Chapter 1: Variables, input/output and arithmetic
* Chapter 2: Object: An introduction
* Chapter 3: Selection structures
* Chapter 4: Iteration structures
 

Buch.png