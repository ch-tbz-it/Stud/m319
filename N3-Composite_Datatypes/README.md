![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# Zusammengesetzte Datentypen

---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:
* Einige nützliche, zusammengesetzte Datentypen (=Klassen) werden vorgestellt:
* Arrays
* Strings
* ArrayList
* Wrapperklassen

---

![Learn](../x_gitressourcen/Learn.png)


# Statische Felder mit fixer Länge (Arrays)

![ToDo](../x_gitressourcen/ToDo.png) To Do: **Studieren Sie folgende Kapitel:**

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.6](../Knowledge_Library/Java_Programmieren.pdf)

![Statische Felder](./x_gitressourcen/Stat_Felder.png)

![Video:](../x_gitressourcen/Video.png) Demo Eindimensionale Arrays: 5 min
[![Erklärung](https://img.youtube.com/vi/ei_4Nt7XWOw/0.jpg)](https://www.youtube.com/watch?v=ei_4Nt7XWOw)

![Video:](../x_gitressourcen/Video.png) Demo Zweidimensionale Arrays: 8 min
[![Erklärung](https://img.youtube.com/vi/alwukGslBG8/0.jpg)](https://www.youtube.com/watch?v=alwukGslBG8)

[W3S Arrays](https://www.w3schools.com/java/java_arrays.asp)

[Merkblatt Statische Felder](./m319 Merkblatt statische Felder.pdf)

(Siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/Arrays.html))

*Für Profis:* [Buch Java ist auch eine Insel: Arrays](https://openbook.rheinwerk-verlag.de/javainsel/04_001.html#u4)

---

# Zeichenketten / Strings

![ToDo](../x_gitressourcen/ToDo.png) To Do: **Studieren Sie folgende Kapitel:**

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.7](../Knowledge_Library/Java_Programmieren.pdf)

![Codierung](./x_gitressourcen/Codierung.png)

[W3S String Methoden](https://www.w3schools.com/java/java_ref_string.asp)

(Siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/String.html))

## Besonderheiten von Strings

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Anhang A.19](../Knowledge_Library/Java_Programmieren.pdf)

[W3S Strings](https://www.w3schools.com/java/java_strings.asp)

[W3S String Methods](https://www.w3schools.com/java/java_ref_string.asp)

*Für Profis:* [Buch Java ist auch eine Insel: Strings](https://openbook.rheinwerk-verlag.de/javainsel/04_004.html#u4.4)

---

# Dynamische Felder (Arraylist)

![ToDo](../x_gitressourcen/ToDo.png) To Do: **Studieren Sie folgende Kapitel:**

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.8.2 bis 8.2.2](../Knowledge_Library/Java_Programmieren.pdf)

![Dynamische Felder](./x_gitressourcen/Dyn_Felder.png)

![Video:](../x_gitressourcen/Video.png) Demo: 6 min
[![Erklärung](https://img.youtube.com/vi/Z-Zs4sgcyv0/0.jpg)](https://www.youtube.com/watch?v=Z-Zs4sgcyv0)

[W3S ArrayList](https://www.w3schools.com/java/java_arraylist.asp)

[Merkblatt Dynamische Felder](./m319 Merkblatt dynamische Felder.pdf)

(Siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/ArrayList.html))

## Spezialitäten der dynamischen Felder

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Anhang A.18](../Knowledge_Library/Java_Programmieren.pdf)


---

# Wrapperklassen

Einige Wrapperklassen werden unter vorgestellt:
[Classes and Objects](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N2-Classes_Objects_Methods#beispiel-wrapper-klasse-integer)

Eine Übersicht zu den Wrapperklassen befindet sich hier:

[W3S Wrapper Klassen](https://www.w3schools.com/java/java_wrapper_classes.asp)

*Für Profis:* [Weiterführende Erklärungen auf Baeldung.com](https://www.baeldung.com/java-wrapper-classes)

---


# Weitere interessante Datentypen (Klassen)


* **BigInteger**: Ein Big-Integer kann beliebig große ganze Zahlen darstellen. Dabei sind 300-stellige Zahlen keine Seltenheit, aber auch kein Problem für JAVA. (siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/math/BigInteger.html))

* **BigDecimal**: Dagegen kann ein Big-Decimal mit beliebig vielen Nachkommastellen umgehen. Die Zahl π auf tausend Nachkommastellen zu berechnen ist mit Java also gut möglich. (siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/math/BigDecimal.html))


---

![CP](../x_gitressourcen/CP.png)

# CheckPoint

* Strings können angewendet werden.
* Eigenheiten von Strings sind bekannt.
* Arrays können angewendet werden (primitive Felder und Arraylist).
* Eigenheiten von Arrays sind bekannt.

