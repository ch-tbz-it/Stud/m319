![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)√

[TOC]

# Abstraktionen mit Parameter


---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:
* Unterschied der zwei Paramaterübergabe-Konzepte. 
* Anwendung der Konzepte bei Variablen mit primitiven und komplexen Datentypen

---

![Learn](../x_gitressourcen/Learn.png)

# Paramaterübergabe:

Um Daten in einem Unterprogramm zu verwenden, werden diese vorzugsweise in der Parameterliste eingetragen und übergeben:

![Video:](../x_gitressourcen/Video.png) 5 min 
[![Tutorial](https://img.youtube.com/vi/2qFNi81dWuI/0.jpg)](https://www.youtube.com/watch?v=2qFNi81dWuI)

> **Merke**:
> 
> * Call-by-Value (Wertübergabe): Kopiert Wert in lokale Variable und dann wird in der Methode/Funktion nur die lokale Kopie geändert!
> * Call-by-Referenz (Variablenübergabe): Referenz auf Original wird übergeben und dann wird in der Methode/Funktion das Original geändert, und somit auch im Hauptprogramm! 

## Call by Value (für primitive Datentypen)

Alle Variablen der **primitiven Typen** werden in JAVA immer **als Wert** übergeben!

Siehe Video oben!

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.5.2.2 -5.2.4](../Knowledge_Library/Java_Programmieren.pdf)


## Call-by-Referenz (für komplexe Datentypen)

Alle Variablen mit **komplexen Datentypen** werden in JAVA immer **als Referenz** übergeben! D.h. eine Veränderung des Inhalts der solchen Variable wird auch im Hauptprogramm wirksam.

![Video:](../x_gitressourcen/Video.png) 5 min 
[![Tutorial](https://img.youtube.com/vi/6pqWEj2XVcQ/0.jpg)](https://www.youtube.com/watch?v=6pqWEj2XVcQ)


> **Achtung**: Eine Änderung der *Referenz* innerhalb der Funktion führt *auch* zur Veränderung der *Referenz* im Hauptprogramm!

---

![Train](../x_gitressourcen/Train_R1.png)

![ToDo](../x_gitressourcen/ToDo.png) To Do:

[W3S Parameterübergabe](https://www.w3schools.com/java/java_methods_param.asp)


---

![CP](../x_gitressourcen/CP.png)

# Check Point
* Primitive Datentypen &#8594; Call-by-Value
* Komplexe Datentypen (Objekte) &#8594; Call-by-Reference *für den Inhalt der Objektvariable*
* Parameterliste und Rückgabewert
* Verhalten der Variableninhalte ausserhalb und innerhalb der Funktionen

Buch.png