![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)√

[TOC]

# Kontrollstrukturen

---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:
* Was ist mit Kontrollfluss gemeint. 
* Die Steuerung des Programmablaufs.
* Welche Bedingungen gibt es in JAVA.
* Welche Möglichkeiten des Kontrollflusses gibt es in JAVA.

---

![Learn](../x_gitressourcen/Learn.png)


# Einführung:

Der **Kontrollfluss** oder **Programmablauf** bezeichnet in der Informatik die zeitliche Abfolge der einzelnen Befehle eines Computerprogramms. Der Kontrollfluss eines Programms ist gewöhnlich durch die Reihenfolge der Befehle innerhalb des Programms vorgegeben, jedoch erlauben Kontrollstrukturen, von der sequenziellen Abarbeitung des Programms abzuweichen. Die **Abarbeitungsreihenfolge** der einzelnen Befehle, welche das Programm vorgibt, wird von Kontrollflussabhängigkeiten festgelegt: Ein nächster Befehl wird entweder dann ausgeführt, wenn der unmittelbar vorhergehende Befehl abgearbeitet wurde, oder wenn ein **Kontrollfluss-Steuerelement** (siehe weiter unten) zum Befehl springt. ([Auszug Wikipedia](https://de.wikipedia.org/wiki/Kontrollfluss))



## Ein Programm besteht aus ...
... **Anweisungen**, die in **Blöcke** ```{ ... }``` zusammengefasst werden können, <br>
und aus **Bedingungsanweisungen**, die den Kontrollfluss steuern.

- Anweisungen sind z.B. `println()`, `inputDouble()`, `sqrt()`, `1+2`, ...
- Bedingungsanweisungen sind z.B. `if ( a > 10 ) { ... } else { ... };`

## Bedingungen (Vergleiche innerhalb der Bedingungsanweisung) 
| Vergleich | Code-Beispiel  | **true** (wahr), wenn der ... |
|:---:|:---:|---|
|  = | (a **==** b)  |  Inhalt von a **gleich (identisch)** Inhalt von b ist |
|  &#8800; | (a **!=** b)  |  Inhalt von a **nicht gleich** Inhalt von b ist |
|  < |  (a **<** b) |  Inhalt von a **kleiner** Inhalt von b ist  |
|  &#8804; | (a **<=** b)  |  Inhalt von a **kleiner oder gleich** Inhalt von b ist  |
|  > |  (a **>** b) |  Inhalt von a **grösser** Inhalt von b ist  |
|  &#8805; | (a **>=** b)  |  Inhalt von a **grösser oder gleich** Inhalt von b ist  |

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.3.3 bis 3.3.2 studieren](../Knowledge_Library/Java_Programmieren.pdf)

![ToDo](../x_gitressourcen/ToDo.png) To Do:
[**Vergleiche** hier studieren](https://www.w3schools.com/java/java_booleans.asp)

---

## 1.Sequenz

**Anweisungen**, bzw. ganze Blöcke werden nacheinander ausgeführt, d.h. als Sequenz behandelt:

![Video:](../x_gitressourcen/Video.png) 1:20 Min
[![Sequenz](./x_gitressourcen/Sequenz.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/michael_kellenberger_tbz_ch/ETrE48c7a_FaF-vhCHA75NEBfkz-hNhTpcf8KrbFgoXSdw?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19&e=uWv8Gi)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.2.1 studieren](../Knowledge_Library/Java_Programmieren.pdf)

Beispiel [Template.zip](../N1-JAVA_Basics/Template.zip): Title > Input > Calculation > Output

```java
public static void main(String[] args ) {

        // Title
        System.out.println("Dreiecksberechnung:");
        out.println();

        // Input
        double a = inputDouble("Geben Sie die Seite a ein: ");
        double b = inputDouble("Geben Sie die Seite b ein: ");

        // Calculation
        double c = Math.sqrt(Math.pow(a,2) + pow(b,2));

        // Output
        out.println("Das Resultat ist: " + c);
    }
```
---

## 2.Selektion
Der Programmfluss kann aufgrund einer Bedingung **verzweigt** werden. 

![Video:](../x_gitressourcen/Video.png) 5:20 Min
[![Selektion](./x_gitressourcen/Selektion.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/michael_kellenberger_tbz_ch/ESef-zpkxhZTU-AUipapABsBrT-Hqi0tJ2VihRdRYve8pQ?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19&e=Bf1ABO)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.3.1 - 3.5 studieren](../Knowledge_Library/Java_Programmieren.pdf)

![ToDo](../x_gitressourcen/ToDo.png) Do To: <br>
[if...else](https://www.w3schools.com/java/java_conditions.asp)<br>
[switch](https://www.w3schools.com/java/java_switch.asp)<br>

---

## 3.Iteration
Eine Anweisung oder ein Block von Anweisungen kann **mehrfach wiederholt** (iteriert) werden. Eine Bedingung entscheidet, wie oft wiederholt wird. Die Bedingung formuliert dabei entweder ein **Abbruch**-Kriterium oder ein **Fortfahr**-Kriterium.

![Video:](../x_gitressourcen/Video.png) 9:24 Min
[![Iteration](./x_gitressourcen/Iteration.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/michael_kellenberger_tbz_ch/EYj6P4DtosVawjUSEtFd4dUBoe3yMPmXc6oBgbiW7KOEmg?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19&e=wMs6jd)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.4](../Knowledge_Library/Java_Programmieren.pdf)

![ToDo](../x_gitressourcen/ToDo.png) Do To: <br>
[while...](https://www.w3schools.com/java/java_while_loop.asp)<br>
[for...loop](https://www.w3schools.com/java/java_for_loop.asp)<br>
[break/continue](https://www.w3schools.com/java/java_break.asp)<br>

---

## 4.Abstraktion
Einzelne (längere) Blöcke, die ein **thematisch abgeschlossenes Problem** lösen,  können zur besseren Übersichtlichkeit in ein Unterprogramm (Prozedur, Funktion, Methode) ausgelagert werden. Dabei wir am Ort der **Auslagerung** ein bezeichnender "Name" hinterlegt, der den Unterprogramm-Sprung einleitet. Das ausgelagerte Unterprogramm wird als solches mit demselben "Namen" gekennzeichnet. 
Der gewählte "Name" sollte einen Hinweis zum Zweck des Unterprogramms geben. 
Dieses Konzept nennt sich "Abstraktion"!

> **Anm**.: Funktionsnamen sind kleingeschrieben und in CamelCase.

![Video:](../x_gitressourcen/Video.png) 4:35 Min
[![Abstraktion](./x_gitressourcen/Abstraktion.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/michael_kellenberger_tbz_ch/ESJGcwLehOtQUCg5ZoZp9KUBikifwXPLU4yFITlG-pZ7Kw?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19&e=ZfwQnI)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.2.4 studieren](../Knowledge_Library/Java_Programmieren.pdf)

Beispiel: [scope.java](../N1-Variables_Constants/scope.java) main >> func() >> main

```java
    public static void main(String[] args ) {

        int var = 2;
        
        System.out.println("Im Main: var=" + var);
        
        // Calls the funtion "func"
        func();
        
        System.out.println("Im Main: var=" + var);
    }

    // function which runs under main
    public static void func() {
        
        int var = 3;
        
        System.out.println("In Func: var=" + var);
    }
```

![ToDo](../x_gitressourcen/ToDo.png) Do To:
[Abstraktionen werden in Java "Methods" genannt ...](https://www.w3schools.com/java/java_methods.asp)

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.5 bis 5.2.1 studieren](../Knowledge_Library/Java_Programmieren.pdf)

> **Anm**.: Zur besseren Unterscheidung werden im Schulkontext <br> "**Methode**" für _Funktionen von/mit Objekten_, <br> "**Funktion**" für _Funktionen ohne Objektbezug_ benutzt.

---

![Train](../x_gitressourcen/Train_R1.png)


## Train 

![ToDo](../x_gitressourcen/ToDo.png) Do To:

### Erweiterung des GallonsConverter mit einer Iteration

Verbessern Sie das Programm *GallonsConverter.java*. Wir wollen nun eine Tabelle mit Umrechnungen ausgeben, beginnend mit 1 Gallone bis 100. Nach jeweils 10 Gallonen soll eine leere Zeile ausgegeben werden. Verwenden Sie dazu eine Variable, die die Anzahl Zeilen zählt.

Überlegen Sie sich, welche Form von Iteration Sie verwenden können.

Hier ist die Vorlage *GallonsConverter*:

```java
public class GallonsConverter {

	public static void main(String[] args) {
	
		//defining variables:
		
		double gallons;
		double litres;
		
		gallons = 10;              //assigns a value
		
		litres = gallons / 3.7854;
		
		// print out using the System library:
		System.out.println(gallons + " gallons is " + litres + " litres.");
	}

}
```


### Celsius – Fahrenheit Berechner

Schreiben Sie ein Programm, das dem Benutzer erlaubt, entweder den Temperatur (Grad) in Celcius oder Fahrenheit zu berechnen. Überlegen Sie sich zuerst, welche Datentypen Sie verwenden sollten.

Geben Sie eine Meldung aus, wenn die Temperatur unter 0&#8451; (Gefrierpunkt), oder 100&#8451; (Siedepunkt) ist!


### Berechnung Distanz Gewitter

Schreiben Sie eine Klasse *GewitterBerechner*, der die Entfernung eines
Gewitters berechnet.

Die Schallgeschwindigkeit in Luft beträgt etwa 344 m/s (Meter pro Sekunde).
Lassen Sie den Benutzer die entsprechenden Sekunden eingeben.

Welche Datentypen verwenden wir für die Variablen?

Wiederholen Sie die Berechnung, wenn der Benutzer dies wünscht (Do ... while)


---

## Weitere Links:
[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.3.6 studieren - Prüfen von Dezimalbrüchen](../Knowledge_Library/Java_Programmieren.pdf) <br> 
[Poster Kontrollstrukturen: AD](./Kontrollstrukturen_AD.png) <br>
[Ausdrücke, Operanden und Operatoren](https://openbook.rheinwerk-verlag.de/javainsel/02_004.html#u2.4) <br>
[Namenskonventionen](https://mein-javablog.de/java-namenskonventionen/)

---

![CP](../x_gitressourcen/CP.png)

# CheckPoint
* Wie wird der Programmfluss gesteuert?
* Welche Bedingungen gibt es in JAVA?
* Wie wird im Programm verzweigt?
* Wie wird ein Block wiederholt?
* Was ist eine Abstraktion? Wie funktioniert sie?

