![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# Variablen und Konstanten

---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:
* Zweck einer Variable, einer Konstante 
* Konzept zur Variablenhandhabung: Deklarierung, Initialisierung, Verwendung
* Gültigkeitsbereich einer Variable
* Welche primitive Datentypen unterstützt JAVA

---

![Learn](../x_gitressourcen/Learn.png)

## Einführung in das Konzept von Variablen

![Video:](../x_gitressourcen/Video.png) 11:05 Min
[![Konzept](./x_gitressourcen/KV.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/michael_kellenberger_tbz_ch/EWoM0aMtu7NeuNilsPDOR_oBf7z8QCSxd_x6lC8FTlzOcw?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19&e=gKXZwC)

[Link zum Poster](./ProgBASICS Variablen V1.3.png)

---

# Was sind Variablen?

Jedes Computerprogramm verarbeitet Daten. Diese Daten können unterschiedlichen
**Datentyps** sein und als solche wiederum verschiedene Werte besitzen. Die Daten stellen das 'Material' dar, das ein Programm verarbeitet.

Variablen werden verwendet, um einen Wert abzuspeichern. Dieser Wert kann auch
überschrieben und somit geändert werden. Sehr oft hat eine Variable einen
Anfangswert, der zu einem späteren Zeitpunkt geändert wird (z.B. nach einer
Berechnung).

## Standarddatentypen: Primitive Datentypen

Je nach Datentyp hat die Variable eine andere Verwendung im Programm. Um z.Bsp.
einfache Berechnungen mit ganzen Zahlen anzustellen, wird der Datentyp
**Integer** verwendet. Um mit Wörter oder irgendwelchen Zeichenketten zu
arbeiten, werden Zeichenketten (**Strings**) verwendet.

In Java gibt es eingebaute Standarddatentypen (sog. **primitive Datentypen**), die man
für Datenmanipulationen verwenden kann.   
In der unteren Tabelle sind die 8 primitiven Datentypen und die zugehörige
Wrapper-Klasse aufgelistet. Neben dem Datentyp-Namen sind in der Tabelle die
Grösse des Datentyps, der Initialisierungswert und der jeweilige
Wertebereich angegeben.

| **Kategorie** | **Datentyp** | **Größe [Bit]** | **Initialisierungs-wert** | **Wertebereich <br> (MIN MAX)** |
|--------|----------|:-------------:|:----------------:|:---------------------:|
| Wahrheitswert | boolean      | 1               | *false*          | *true*, *false*          | 
| Bytezahl | byte         | 8               | 0                | -128 … +127              | 
| Kleine Ganzzahl | short        | 16              | 0                | -32'768 … +32'767             | 
|Mittlere Ganzzahl | int          | 32              | 0                | -2'147'483'648 … +2'147'483'647  | 
| Grosse Ganzzahl | long         | 64              | 0                | -2^63 … +(2^63)-1  <br> = <br> -9.2233…e18  … +9.2233…e18 | 
| Reelle Zahl mit 7 Stellen Genauigkeit | float        | 32              | 0.0f             | ±1.4…×10^-38 … ±3.4…×10^+38   | 
| Reelle Zahl mit 14 Stellen Genauigkeit <br> (doppelte Genauigkeit) | double       | 64              | 0.0              | ±4.9…×10^-324 … ±1.8…×10^+308 | 
| Ein Buchstabe aus dem UTF-16 Charset <br> (mit ' begrenzt!)| char         | 16              | '\\u0000' | '\\u0000' … '\\uFFFF' <br> Beispiele: 'a' '€'   | 

**Codebeispiel:** (Siehe auch Wrapper-Klasse weiter unten)

```java
	System.out.println("Double min: " + Double.MIN_VALUE);        
	System.out.println("Double max: " + Double.MAX_VALUE);        
	System.out.println("Float  min: " + Float.MIN_VALUE);        
	System.out.println("Float  max: " + Float.MAX_VALUE);
```
> **Anm.**: [char >> Unicode mit max. 16 Bit (UTF-16)](https://de.wikipedia.org/wiki/Unicode) <br>


## Deklarierung, Initialisierung und Verwendung

Variablen benötigen stets einen **Datentyp** und einen **Namen**.

Beispiel, wie eine Variable in einem Programm eingeführt (**Deklarierung**) wird:

```int zahl; ```

Ich kann der Variable einen expliziten Wert zuweisen, indem ich den Wert nach einem
Gleichzeichen hinschreibe:

```zahl = 42;```

Die *erstmalige* Zuweisung eines Wertes an eine Variable wird als
**Initialisierung** (oder **Definition**) bezeichnet. Sie kann *zusammen* mit der Deklaration oder
*getrennt* (wie oben) davon erfolgen.

Änderung des Wertes einer Variable:

```java
double preis = 120;   // Deklaration und Initialisation
preis = preis * 1.11; // Veränderung des Inhaltes
```

Hier wird die Variable *preis* zuerst **deklariert** und sogleich mit dem Wert 120 **initialisiert**. Anschliessend erhält die Variable einen neuen Wert nach der Kalkulation mit 1.11.

## Was ist eine Konstante?

Konstanten sind ähnlich wie Variablen; mit dem Unterschied, dass der Wert
**nicht mehr verändert werden kann** (= er bleibt konstant). Üblicherweise werden Konstanzen **GROSS** geschrieben, mit "_" als Trenner von Worten:

Die Konstante wird mit dem Schlüsselwort **final** bezeichnet:

```java
final char CONST_C = ‘c’;
```

Die Konstante muss auch **sofort mit einem Wert versehen** (initialisiert) werden, d.h. ```final int CONST_INT_NULL;``` ist nicht zulässig, d.h. der Compiler wird eine Fehlermeldung liefern und eben nicht den Inhalt =0 setzen!

Konstanten machen dann Sinn, wenn sich der Wert nicht ändert, z.B. bei der Zahl *pi* oder bei einem Umrechnungsfaktor, den man einmal im Programm festlegt. 

# Gültigkeitsbereich

![Gültigkeitsbereich](./x_gitressourcen/Gueltigkeitsbereich.png)

## Was sind lokale Variablen (oder Konstanten)?

Lokale Variablen werden meist zum Zwischenspeichern von Werten verwendet, etwa
als Zählvariablen innerhalb einer Kontrollstruktur z.B. ```for (int i = 1; ...)```, als Variablen innerhalb einer Methode ```void main () { float x = 1.23; ... )```, also allgemein innerhalb eines Blocks ```{ int a = 3.14; ... }```. 

Sie werden als «lokal» bezeichnet, da sie von ausserhalb der Methode, des Blockes  oder Kontrollstruktur nicht verwendet werden können (nicht existent, d.h. nicht sichtbar).

Beispiele i und x:

```java
	for (int i = 0; i < list.length; i++){ // i ist eine lokale Variable
	   ... 
	   float x = 1.123;                    // x ist auch eine lokale Variable
	   ...
	   System.out.println(i + x + ": " + list[i]);
	   ...
	} 
	...
	//Folgende Anweisung würde ein Compilerfehler erzeugen, da die lokale Variable i nicht mehr existent/sichtbar ist: "Cannot find Symbol"
	System.out.println(i);
```
**Erklärung:**
Oben wird lokal in der for-Schleife die Variable i deklariert und sogleich mit
dem Wert 0 zugewiesen (`int i = 0;`). Die Variable wird als Zähler (=Index) verwendet, um jedes Element aus einer Liste auszugeben. 

Die Variable x wird bei jedem Schleifendurchgang neu deklariert, initialisiert, ausgelesen und wieder verworfen.

Ausserhalb des Deklarations-Blocks ist die lokale Variable nicht existent!

## Was sind "globale" Variablen (oder Konstanten)?

"Globale" Variablen können im ganzen Programm (bei JAVA in der ganzen Klasse) aufgerufen und verwendet werden. Sie sind nicht nur in einer Funktion (oder Methode) oder Kontrollstruktur gültig. Um eine Variable "globale zu machen, muss sie direkt zu Beginn des Klassenblocks deklariert werden. (Sie heissen auch Instanzvariablen oder (mit static) Klassenvariablen).

Beispiel:

```java
class Test {

    // Globale Variabeln: (Variablenliste)
	static int zahl1 = 24; // globale Klassenvariable 
	static int zahl2;          // globale Klassenvariable
	
	static void main() {
	    
	   zahl2 = zahl2 + 2;  // Inhalt wird verändert
	   System.out.println(zahl1 + zahl2); // 24 + 2 => 26
	
	}

}
```

Hier werden die Variablen **zahl1** und **zahl2** unmittelbar nach der Klassenbezeichnung deklariert und die Variable **zahl1** gleich mit 24 explizit initialisiert (zahl2 wird implizit vom Compiler mit dem Standardwert 0 initialisiert). Die Wertzuweisung kann dann später irgendwo im Programm erfolgen ... 

> **WICHTIG!** Eine "globale" Variable wird von einer lokalen Variable im lokalen Gültigkeitsbereich **überdeckt** (= nicht mehr *direkt* ansprechbar), **wenn** sie den **gleichen Namen** hat. 


![Train](../x_gitressourcen/Train_R1.png)
![ToDo](../x_gitressourcen/ToDo.png) To Do: 

1. Bauen Sie die obig gewonnen Erkenntnisse in ihre Programme des Topics [Java Basics](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N1-JAVA_Basics?ref_type=heads#ein-zweites-separates-programm-im-projekt-erstellen) ein ...
2. [W2School Numbers](https://www.w3schools.com/java/java_howto_add_two_numbers.asp)


## Demo Gültigkeitsbereich:

![Learn](../x_gitressourcen/Learn.png)

![Video:](../x_gitressourcen/Video.png) 9:40 Min
[![Gültigkeitbereich](./x_gitressourcen/Gb.png)](https://tbzedu-my.sharepoint.com/:v:/g/personal/michael_kellenberger_tbz_ch/EWh8RlUC5FFd-s4u-mFm-yQBJSDn94r_xgZU_3X7GVeirg?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJTdHJlYW1XZWJBcHAiLCJyZWZlcnJhbFZpZXciOiJTaGFyZURpYWxvZyIsInJlZmVycmFsQXBwUGxhdGZvcm0iOiJXZWIiLCJyZWZlcnJhbE1vZGUiOiJ2aWV3In19&e=p23AKm)

Code: [Scope.java](./scope.java)
<https://www.w3schools.com/java/java_scope.asp>


![DIY](../x_gitressourcen/Train_D1.png)

![ToDo](../x_gitressourcen/ToDo.png) To Do: [W3S Scope](https://www.w3schools.com/java/java_scope.asp)

---

![Learn](../x_gitressourcen/Learn.png)

# Primitive vs. komplexe Datentypen

Da Java eine objektorientierte Programmiersprache ist, werden hauptsächlich **Klassen** und deren Objekte zur Verarbeitung im Programm benützt. ([Siehe dazu D2: Klassen und Objekte](../N2-Classes_Objects_Methods)). Die primitiven Datentypen wie **int** usw. wurden aber aus Gründen Einfachheit von der nicht-objektorientierten Sprache C übernommen und beibehalten. 

Oft werden die Daten der primitiven Datentypen aber auch als Objekte gebraucht ... darum gibt es zu jedem primitiven Datentyp eine sog. **Wrapper-Klasse**, die diese Daten in ein Objekt "einpackt": 

| **Kategorie** | **Standard-Datentyp** |  **Wrapper-Klasse**  |
|--------|----------|----------------|
| Wahrheitswert | boolean      |  java.lang.**Boolean**  |
| Bytezahl | byte         |  java.lang.**Byte**      |
| Kleine Ganzzahl | short        |              java.lang.**Short**     |
|Mittlere Ganzzahl | int          | java.lang.**Integer**   |
| Grosse Ganzzahl | long         |  java.lang.**Long**      |
| Reelle Zahl mit 7 Stellen Genauigkeit | float        |  java.lang.**Float**     |
| Reelle Zahl mit 14 Stellen Genauigkeit <br> (doppelte Genauigkeit)| double       | java.lang.**Double**    |
| Ein Buchstabe aus dem UTF-16 Charset (mit ' begrenzt!)| char         | java.lang.**Character**|

Es handelt sich also um eine Hilfsklasse, die man bei Bedarf statt dem primitiven Datentyp verwenden kann. Wrapper-Klassen bieten Methoden (Funktionen) zu den einzelnen Datentypen an, z.Bsp. um die maximalen oder minimalen Werte auszugeben:

```java 
System.out.println("Integer min: " + Integer.MIN_VALUE);
System.out.println("Float   max: " + Float.MAX_VALUE); 
```

### Der Datentyp String
**String** ist ein spezieller Datentyp, da es sich hier bereits um eine Klasse
handelt (= komplexer Datentyp) und nicht um einen primitiven Datentyp. Wie bei den Wrapper-Klassen, hat String eingebaute Methoden, die man verwenden kann. ([siehe dazu D3: Komplexe Datentypen](../N3-Complex_Datatypes))

---

# Quellen und weitere Erklärungen:

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap. 2.2, 2.3 A.15.2](../Knowledge_Library/Java_Programmieren.pdf)

<https://javabeginners.de/Grundlagen/Variablen.php>

<https://de.wikibooks.org/wiki/Java_Standard:_Primitive_Datentypen>

<https://www.programmierenlernenhq.de/java-grundlagen-die-2-datentypenarten-in-java-und-ihre-verwendung/>

<https://www.w3schools.com/java/java_wrapper_classes.asp>

<https://www.java-tutorial.org/datentypenundvariablen.html>

---
![CP](../x_gitressourcen/CP.png)
# Checkpoint
* Konzept der Variabeln (Konstanten) ist bekannt. <br> >> Behälter mit Name, Datentyp, Grösse und (fixem) Inhalt.
* Erkenne Deklarierung, Initialisierung und Verwendung von Variablen (Konstanten).
* Kann den Gültigkeitbereich "setzen". (Position der Deklarierung im Code!).
* Wann wird eine "globale" Variable übderdeckt?

