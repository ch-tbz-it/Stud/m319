![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# Übung: 

Voraussetzungen: JDK (SDK) von Java wurde richtig installiert. Der Pfad befindet sich (z.B.) hier: 

## Mein erstes Java Propgramm auf der Konsole mit dem Scanner Template

Versuchen Sie mit dem [ScannerTemplate](./ScannerTemplate.java) Ihr erstes Programm zu machen. Im aller ersten Schritt machen Sie Ihr Code in die `main`-Methode (PS1) hinein.

Erstellen Sie bei sich in Ihrer IDE eine Klasse (PS2) "ScannerTemplate.java" und kopieren Sie den Code da hinein und erweitern entsprechend.

_PS1: Wenn Sie in Projekten arbeiten, werden Sie nie mehr mit einer main-Methode direkt zu tun haben. Die ist zwar in jedem Java-Projekt vorhanden, aber alle weitere Funktionen und auch Ihre weitere Entwicklungen sind in, von der main-Methode aufgerufenen Programmteilen._

_PS2: (Die Klasse und die Datei muss in Java den gleichen Namen haben, Bei C# von Microsoft ist es etwas anders)_
