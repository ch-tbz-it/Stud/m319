![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)√

[TOC]

# JAVA Basics


---

## Wie lerne ich zu Programmieren?

... bzw. wie lerne ich eine Programmiersprache?

**Antwort** <br>
Wie bei einer Fremdsprache …

![Programmieren](./x_gitressourcen/ProgrLernen.jpg)


---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziel:
* Wie wird ein Programm erstellt.
* Grundbegriffe von JAVA 
* Aufbau eines Programms
* Erste Schritte mit Java (Template)

--- 

![Learn](../x_gitressourcen/Learn.png)

## Vom Programmtext zur Ausführung

Ein Quellcode wird grundsätzlich entweder "kompiliert" oder "interpretiert":

![Comp_Inter](./x_gitressourcen/Comp_Inter.png)

**Entwicklungszeit** Das Programm ist in der Entwicklung (Editor) <br>
**Laufzeit** Das Programm wird ausgeführt <br>
**Compiler** sind Programme, die den - mit einem Editor geschriebenen - Sourcecode (Programmtext) in Maschienencode (MC) *übersetzen*. <br> Vorteil: Schnelle Ausführung des Programms <br>
**Interpreter** sind Programme (sog. "Hosts"), die den mit einem Editor geschriebenen Sourcecode  *direkt ausführen*. <br> Vorteil: Flexible Ausführung und oft auf versch. Plattformen ausführbar!


Moderne Programmiersprachen wie Java benutzen beide Vorteile:

![Java Programmieren](./x_gitressourcen/JavaCompInter.jpg)

Der **JAVA-Compiler** ist ein Programm (javac.exe), das .java-Quellcode in einen *universellen, plattformunabhängigen Byte-Code* übersetzt. (.class / .jar) <br>
Der **JAVA-Interpreter** führt den universellen Byte-Code direkt in der *virtuellen Maschine der entspr. Plattform (OS)* (java.exe in der JVM auf WINDOWS oder MAC oder LINUX) aus.

>[>>> Wollen Sie mehr wissen? (Guru) <<<](https://www.guru99.com/java-virtual-machine-jvm.html)

---


![Train](../x_gitressourcen/Train_R1.png)

![ToDo](../x_gitressourcen/ToDo.png) ToDo:

Erstellen Sie ihr erstes "Hello Welt"-Java-Programm in einem einfachen Editor und bringen Sie es wie oben dargestellt mit den beiden (wichtigsten) Tools im CMD oder Powershell zum Laufen...

> **Voraussetzung**: JDK (SDK) von Java wurde richtig installiert. Der Pfad zur JVM ist in der Umgebungsvariable %PATH% abgelegt.

```java
public class MeinErstes {
	public static void main(String[] args) {
		System.out.println("\n Hallo Welt \n");
	}
}
```

1. Einen einfachen Texteditor (z.B. `Editor`, nicht die IDE!) öffnen, obigen Java-Code rein kopieren und als `MeinErstes.java` lokal (nicht im OneDrive) speichern: z.B. `C:\Users\Ich\`
2. Powershell- oder CMD-Konsole öffnen und ins Verzeichnis wechseln: `> cd C:\Users\Ich\`
3.  Mit dem Java-Compiler kompiliert: `> javac.exe .\MeinErstes.java` <br> Es entsteht die Datei `MeinErstes.class`.
3. Zuletzt mit der virtuellen Maschine von Java (JVM) ausführen: `> java.exe .\MeinErstes.java`

![Powershell Fenster](./x_gitressourcen/MeinErstes.png)

So nun wollen wir aber zurück zu unserer IDE gehen und eigene, sinnvollere Programme schreiben und sie dort (einfacher) laufen lassen! 

---

![Learn](../x_gitressourcen/Learn.png)

## JAVA Grundelemente:

**Die Struktur eines (einfachen) Programms ist folgendermassen aufgebaut:**

[Programm Aufbau (W3S)](https://www.w3schools.com/java/java_syntax.asp)

![Struktur einfach](./x_gitressourcen/Struktur_einfach.jpg)

**Projekt**: In einer IDE wird jedes Programm als Projekt verwaltet. Um ein neues Programm zu schreiben, müssen sie darum ein Projekt eröffnen. <br>

**Package**: Oft ist ein Programm in ein Paket geschnürt, welches dem umgekehrten Domain Name der Firma entspricht. Die Paketbezeichnung wird als erste Zeile ins .java-File geschrieben. Über Pakete können Sie ein Projekt strukturieren. <br>

**import**: Bestehende Programm-Bibliotheken laden, um auf deren Funktionen und Objekte zugreifen zu können. <br>

**class static _name_**: Jedes Programm muss in einer Klasse eingebunden sein. Die Klasse muss wie die Java-Datei heissen und umgekehrt.<br>

**public static void main ()**: Hier beginnt das Hauptprogramm ...  <br>

**code**: ...und der Code darin wird ausgeführt.


### So siehts mit Java-Code aus: (Siehe Template weiter unten!)

*Beachten Sie, dass der Kommentar nach den Zeichen `//` nicht ausgeführt wird.*

```java
// Defines package
package ch.tbz;

// Imports libraries
import static java.lang.System.*;   // System-IO Library for abbriviation purpose to static functions
import static java.lang.Math.*;     // Mathematic Library for abbriviation purpose to static functions
import java.util.*;                 // Random is part of this library

// Imports Input functions
import static ch.tbz.lib.Input.*;       // All the input-functions can be used now!

//Every program must be placed in a class ...
public class Main {

    // Our main function which runs the program12
    public static void main(String[] args ) {

        // Title
        System.out.println("Dreiecksberechnung:");
        out.println();

        // Input
        double a = inputDouble("Geben Sie die Seite a ein: ");
        double b = inputDouble("Geben Sie die Seite b ein: ");

        // Calculation
        double c = Math.sqrt(Math.pow(a,2) + pow(b,2));

        // System.Out function (Short version)
        out.println("Das Resultat ist: " + c);

        // Use function Random to generate a random Number
        Random random = new Random();
        //Calling the nextInt() method
        System.out.println("Hier noch eine zufällige Ganzzahl: " + random.nextInt());

    }
}
```

*Das Programm liest zwei Zahlen ein (Seiten a und b eines Dreiecks) und berechnet daraus die Seite c. <br> Zusätzlich wird eine Zufallszahl generiert und ausgegeben.*

---

![Train](../x_gitressourcen/Train_R1.png)

![ToDo](../x_gitressourcen/ToDo.png) ToDo :

# "TBZ Projekt" in die IDE (IntelliJ) einbinden mit dem Template 

**Dieses Template vereinfacht die rel. komplexen Vorarbeiten beim Erstellen eines Projektes. In diesem Modul sollten alle Programme in diesem Template als einzelne Java-Dateien abgelegt und gestartet werden. Die Lehrperson kann Ihnen dabei helfen ... <br>
Ebenso stellt das Template eine INPUT-Library (`..\Template\src\ch.tbz\lib\input`) zur Verfügung, um bei Eingaben die einfachen Funktionen `inputString()`, `inputInt()`, usw. verwenden zu können, anstelle des umständlichen Scanners!**

1. Laden Sie die Datei ["Template.zip"](./Template.zip) herunter. *(Nicht den Ordner "Template!")*
2. und **entzippen** Sie es in ihrem m319-Arbeitsverzeichnis. <br> Hinweis: Allenfalls müssen Sie noch den Template-*Unterordner* aus dem Template-*Ordner* rausnehmen!
3. **Öffnen** (Menü **>File>Open**) Sie den **Ordner "Template"** in der IDE (Achtung: Also keine Datei öffnen!)
4. und studieren Sie den Code der **main-Funktion**. ![IDE Projekt](./x_gitressourcen/IDE Projekt.png)
5. Um das Main zu starten, muss das Projekt wissen, welches SDK zu verwenden ist: Im Menu mit **>Build>Main** das Projekt zusammenstellen (CTRL-F9)
6. Ein Popup erscheint: <br> ![Configure](./x_gitressourcen/Configure.jpg)
7. Klicken sie auf "Configure..." und wählen Sie unter Projekt Ihr installiertes SDK (z.B. Oracle OpenJDK 23) aus ... <br> ![SDK](./x_gitressourcen/Projekt-SDK.jpg) 
8. ... und dann **[Automatic Download]**
9. Versuchen Sie nun das Programm im Menu mit **>Run>Main** nochmals zu starten (CTRL-R). Es erscheint unten eine Console für die programmierten Ein- und Ausgaben. **Gebe Sie ein paar Zahlen ein und versuchen Sie das Programm zu verstehen!**

![Run>Main()](./x_gitressourcen/RunMain.png)

**Java SDK Einstellung:** <br>
Falls Sie später die **Einstellungen** nochmals anpassen müssen, erreichen Sie das Konfigurationsfenster für das ganze Modul über das **Kontext-Menu** vom Projekt "Template": **>OpenModule Settings (F4)** <br>
![Run selector](./x_gitressourcen/ConfigModule_F4.png) <br>
 ... bzw. das Konfigurationsfenster für eine einzelne Klasse über den **Run-Class-Selector** oben rechts: <br> ![Run selector](./x_gitressourcen/Config.png) 

<br>

---

Hier die Struktur vom Template mit der Klasse "main" und mit der Library "Input": 

![Struktur Template](./x_gitressourcen/Struktur_template.jpg)

Die Bibliotheks-Klasse **Input** (in ```package ch.tbz.lib```) stellt **Funktionen zur Eingabe von Werten** bereit: <br> 
```=inputInt("Msg:")```, <br> 
```=inputFloat(...)```, <br> 
```=inputString()```, <br>
`=input...()` <br> 



![Train](../x_gitressourcen/Train_R1.png)

![ToDo](../x_gitressourcen/ToDo.png) ToDo:

Studieren Sie die *public Funktionen* der Bibliotheks-Klasse **Input**!

[JavaDoc zur Bibliotheks-Klasse **Input**](./Input_Lib.pdf)

Studieren Sie nun den Code in der **Main Klasse** im Template. Erkennen Sie die Funktionsaufrufe zur Bibliotheks-Klasse **Input**?

---

![DIY](../x_gitressourcen/Train_D1.png)

### Erste Übungen für den Einstieg: Einfache Berechnungen selber programmieren

![ToDo](../x_gitressourcen/ToDo.png) ToDo:

Haben Sie bemerkt? Eingabe, Berechnung und Ausgabe erfolgten in der main-Funktion: Ändern Sie nun den Code darin etwas ab! Z.B. indem Sie Addition, Subtraktion, Multiplikation, Division, Wurzel und  Potenz von zwei ganzen Zahlen berechnen. Seien sie kreativ ...<br>
Ändern Sie auch den Datentyp der Variablen: z.B. zwei **Int**(-eger) Variablen. <br>
Verwenden Sie für die Eingabe `inputInt("...")` der importierten Input-Bibliothek. (**Nicht den Scanner!**) 

[![Buch](../x_gitressourcen/Buch.png)Unterlagen Kap.1.1 bis 1.5.2](../Knowledge_Library/Java_Programmieren.pdf)

[Siehe auch "TOPIC Variables & Constants"](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N1-Variables_Constants)

[![Buch](../x_gitressourcen/Buch.png) Unterlagen Kap. 2.2 Seite 30](../Knowledge_Library/Java_Programmieren.pdf#)


## Ein zweites separates Programm IM PROJEKT erstellen

>**Turbo-Tipp: Verwenden Sie von nun an nur dieses Template und die Input-Funktionen für alle weitere Aufgaben... (Und bitte NICHT den Scanner einsetzen!)**

> 1) Erstellen Sie zu jeder zukünftigen Aufgabe eine neue Klasse **in diesem Template**: <br>
> 2) **Kontextmenü auf Eintrag ">ch.tbz" > New >Java Class** und einen treffenden Namen (z.B: "Berechnung" \*) der neuen Klasse angeben (CamelCase) <br>
> 3) **Ersetzen** Sie den automatisch erzeugten Inhalt z.B. mit dem Inhalt der "main"-Klasse vom Template (erspart viel Tipparbeit) <br>
> 4) und ändern Sie den Namen der **class** von "Main", um in den gewählten Namen von Schritt 2 \* ("Berechnung") <br>
> 5) Starten Sie diese neue Klasse, indem Sie das **Kontextmenü** über der Klasse öffnen: <br>
> 6) "**>Run**" <br>  
> 
> Von nun an erscheint die Klasse im Run-Menü an der oberen rechten Ecke und kann von dort gestartet werden!
> 
> Lassen Sie sich von der Lehrperson coachen, wenn Sie hierbei stecken bleiben!!!

![Neue Aufgabe starten](./x_gitressourcen/Run_Berechnung.png)

### Hier ein Beispiel eines Gallonen-Konverters:

Kopieren Sie folgenden Code in die Klasse Berechnung. Studieren Sie das Programm und lassen Sie es laufen.

```java

public class Berechnung {

   // Die Funktion main in einer Klasse wird automatisch gestartet
	public static void main( String[] args ) {
		
		// Variablen bereitstellen und nullen:
		double gallons = 0;
		double litres = 0;
		
		// Einen Wert speichern:
		gallons = 10;   
		
		//Das Result berechnen:             
		litres = gallons / 3.7854;
		
		// Ausgabe des Resultats auf einer Zeile:
		System.out.println(gallons + " gallons is " + litres + " litres.");
	
	}
}
```

### Name und Adresse einlesen und ausdrucken

Schreiben Sie weiter einen Code, in dem Name und Adresse ausgegeben werden. Fordern Sie den User auf, seinen Namen und die Adresse einzugeben. Wir verwenden dazu zwei String-Variablen und für die Eingabe inputString("..."). (Nicht den Scanner!)


## Weitere Links:
[Ausdrücke, Operanden und Operatoren](https://openbook.rheinwerk-verlag.de/javainsel/02_004.html#u2.4) <br>
[W3S Mathe Bibliothek](https://www.w3schools.com/java/java_ref_math.asp)


---
![CP](../x_gitressourcen/CP.png)
# Checkpoint
* Sie kennen den Weg vom Quelltext zum Output in der JAVA-Welt.
* Sie kennen die (drei) grundsätzlichen Konzepte der Programmerstellung (Übersetzung).
* Was ist ein "Ausdruck", ein "Literal", ein "Operator" und ein "Bezeichner".
* Kenne den strukturellen Aufbau eines JAVA Programms.
* Ein Projekt in der IDE mit "TBZ Template" eröffnet und ausgeführt.
* Kann ein einfaches Programm nachvollziehen.
* Kann mit eingegebenen Zahlen einfache Berechnungen kalkulieren.


