# Learning Rules:

Um eine reibungsloses und ungestörtes, gemeinsames Lernen zu ermöglichen werden folgende Regeln (zus. zu den Hausregeln) engeführt:

1. **Pünktlichkeit**: Jeder Lernende erscheint im und verlässt Zimmer pünktlich. &#8594; Absenz. <br> Abgemachte Meetings werden eingehalten.
2. **Sozialer Umgang**: Wir pflegen einen respektvollen Umgang. &#8594; Feedback wird konstruktiv aufgenommen (keine Machtkämpfe, kein Mobbing)
3. **Kooperatives Lernen**: Wir unterstützen einander im gegenseitigen Lernprozess. <br> Wir planen Zeit dafür ein. <br> Wir stellen unsere Ressourcen einander zur Verfügung! 
4. **Lärmpegel**: Wir reden im Flüsterton miteinander. <br> Keine emotionalen Ausbrüche, kein Rumschreien! &#8594; Zimmerverweis
5. **Pausen**: Die offizielle Pausenzeit beträgt 40 Min pro 4 Lekt. (~20%!)
<br> Die Pausen können selbstständig eingeteilt werden, müssen aber dem Ablaufrahmen angepasst sein (Input/Meetings). 
<br> Die Pausen sind ausserhalb des Zimmers abzuhalten.
<br> Pausen sind dazu da den Lernprozess setzen zu lassen. &#8594; kein früheres gehen!
6. **Handynutzung**: Handy ist in Rucksack/Tasche verstaut &#8594; Pause ausserhalb des Zimmers
7. **Gamen, Face-book, etc.**: Unproduktive Aktivitäten sind klar als Pause zu deklarieren, d.h. auch ausserhalb des Zimmers &#8594; Zimmerverweis
8. **Weitere ?**: Können auch später bei Bedarf eingefügt werden!
9. **Missachtung von Regeln**: Konsequenzen werden widerspruchslos umgesetzt. (Kein Debattieren)
<br> (Bei Uneinigkeiten bez. Regeln entscheidet der LP)
