![TBZ Logo](../x_gitressourcen/tbz_logo.png)√

[TOC]

# Lernenden-zentriertes Lernen: SOL

---

![Lernziel](../x_gitressourcen/LZ.png)


### Lernziele:
* Wozu SOL im Unterricht?
* Welche Rollen gibt es und was sind Ressourcen?
* Einführung in die Begriffe, die beim SOL relevant sind.

---

![Learn](../x_gitressourcen/Learn.png)

**Lehrerzentriertes Lernen**: Ist ein Führungsstil und eine Form des Unterrichts, bei denen alle wesentlichen Impulse, Aktionen und Entscheidungen von dem Lehrer oder der Lehrerin ausgehen: (Didaktisches Dreieck im Schulzimmer)

![SOL](./x_gitressourcen/LZL.jpg)


Wenn Lernende bei vorgegebenen Inhalten und Zielen ihr eigenes Lernen selbst steuern und Entscheidungen über die Art und Weise ihrer Lernorganisation fällen, so spricht man vom **selbstorganisierten Lernen**, sprich "**SOL**".

![SOL](./x_gitressourcen/SOL.jpg)
Innerhalb des vorgegebenen Rahmens ist individuelles Lernen möglich.


## Wozu?

Die Anforderungen der Wirtschaft für neue Berufsleute haben sich gegenüber der Vergangenheit geändert. Die Lehr- und Lernformen müssen sich auch anpassen und neben den **Fachkompetenzen** (Wissen etc.) müssen auch **überfachliche Kompetenzen** mit einbezogen und trainiert werden.


## Rollen

* Der Lernende wird zum "**Entdecker**", der seinen eigenen **Lernprozess** steuert. Der Lernende arbeitet in einem **Tandem-Team**, d.h. jede(r) hat einen Partner(in).
* Die Lehrperson wird zum "**Coach**", die den Lernprozess begleitet.
* Die Lehrperson wird zum "**Experten**", die **Kompetenzen** abnimmt.

## Ressourcen (Quellen)
* Die **zentrale Plattform** (Miro) dient als Drehscheibe für Kommunikation, Planung, Austausch.
*Die Ziele sind in einem **Kompetenzraster** formuliert und indiziert.
* **Lern-Ressourcen** dazu (Inputs, Unterlagen, Übungen, etc) sind online (GitLAB, MS-Stream, YT, etc.) verfügbar und referenziert.
* Das **Vorwissen** des Lernenden.
* Das Wissen der Klasse soll in **Kooperation** genutzt werden können.


---

# Begriffe:

## Kompetenz:
Gemeint ist die Fähigkeit und Fertigkeit, in den genannten Gebieten Probleme zu lösen, sowie die Bereitschaft, dies auch zu tun.
Im Allgemeinen sind sachlich-kategoriale, methodische und selbststeuernde Elemente verknüpft, einschließlich ihrer Anwendung auf ganz unterschiedliche Gegenstände.
-> Umgangssprachlich: „Jemand ist kompetent“ 

 KURZ:  **= Wissen + Können + Anwenden**

![Kompetenzen](./x_gitressourcen/Kompetenzen.jpg)

### Handlungskompetenz:
„Handlungskompetenz wird verstanden als die Bereitschaft und Befähigung des Einzelnen, sich in beruflichen, gesellschaftlichen und privaten Situationen sachgerecht durchdacht sowie individuell und sozial verantwortlich zu verhalten.“ 
Beinhaltet Lern- und Methodenkompetenz, die wiederum als Voraussetzung Fach-, Sozial- und Selbstkompetenz haben!

### Handlungsziel
Messbaren Beschreibung eines Zustandes und/oder Produkts.
Zur Erreichung dieses Ziels müssen aktive Handlungen ausgeführt werden, die durch verschiedenen Kompetenzbereiche charakterisiert werden können.

Handlungsziele werden oft beschreibend formuliert: „Ich kann ...“, „Ich habe ...angewandt“, „Ich weiss ... „

### Kompetenzraster
Ein Raster mit in Kompetenzfelder eingeteilte Handlungsziele. 

![](./x_gitressourcen/KR.png)

**Zweck des Kompetenzrasters:**

* Zielformulierung für selbstorganisiertes Lernen
* Inhaltsbestimmung für Lernsequenzen
* Orientierungshilfe zur Lernstrategieentwicklung (Offene Fragen)
* Vorgaben zur Lernzielkontrolle 

Einteilung in Fachgebiet, Themenbereiche, Schwierigkeitsgrad, Aufwand, Zeitbedarf, Wissensstand, etc. möglich.

Jedes erfolgreich abgeschlossene Kompetenzfeld ergibt summierte Punkte, um am Ende eine abschliessende Note zu evaluieren.


### Lern- und Nachweis-Portfolio

Ein "Portfolio" ist eine Sammlung von "**Daten und Informationen**", die aufgrund des Lernprozesses entstanden (**Urheberrecht**!) oder relevant sind. 

Wir unterscheiden das Lern-Portfolio und das Nachweisportfolio

1. **Lern-Portfolio** <br>
Siehe [Lern-Portfolio](../N0-Portfolio/README.md). In der Praxis wird ein "**persönliches**", digitales Tool verwendet: OneNote, EverNote, Confluence, Miro, GitLAB, ... oder ein LMS. Die Lehrperson erhält einen Link mit Schreibrechten.

2. **Nachweis-Portfolio** <br>
In der Regel eine **strukturierte Ablage** wie Sharepoint oder GitLAB. Die Lehrperson erhält einen Link mit Schreibrechten.


### Kompetenznachweis und Beleg
1. Handlung, bei der eine Lernzielkontrolle durchgeführt wird, um die geforderten Handlungskompetenzen nachzuweisen. 
&#8594; Fachgespräch, Demonstration, Präsentation, Cast anhand eines "**Belegs**" (&#8594; Datei)!
2. Bestätigung, dass die geforderten Handlungskompetenzen ausgewiesen sind! D.h. der Beleg ist im Nachweis-Portfolio abgelegt und im **Punkteraster** als **PASS** markiert!


![Belege](./x_gitressourcen/Belege.jpg)![Punkte](./x_gitressourcen/Punkte.png)

---


&#8594; [Learning Rules](./SOL_Learning_Rules.md)

---

![CP](../x_gitressourcen/CP.png)

# CheckPoint
* Was ist ein **Tandem**?
* Aus welchen Kompetenzen bildet sich die **Handlungskompetenz**?
* Was ist der **Unterschied** zwischen einem Lern- und einem Nachweis-**Portfolio**?
* Was ist ein **Beleg** und wozu wird er verwendet?
* Wann ist ein **Nachweis** abgenommen?
* Welche **Regeln** gelten im Schulzimmer?







